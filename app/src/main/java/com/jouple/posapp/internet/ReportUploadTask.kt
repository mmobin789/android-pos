package com.jouple.posapp.internet

import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.http.FileContent
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import java.io.File


/**
 * Created by macbook on 04/01/2018.
 */
class ReportUploadTask(private val fileType: FileType, private val googleAccountCredential: GoogleAccountCredential, private val file: File, private val listener: OnFileUploadListener) : AsyncTask<Void, Void, com.google.api.services.drive.model.File?>() {
    private var authIntent: Intent? = null

    enum class FileType(val type: String) {


        Excel("application/vnd.ms-excel"), PDF("application/pdf");

        override fun toString(): String {
            return type
        }


    }

    override

    fun onCancelled(result: com.google.api.services.drive.model.File?) {
        Log.e(javaClass.simpleName, "Report Upload Canceled")
    }

    override fun onPreExecute() {
        Log.i(javaClass.simpleName, "Report Upload Task initiated")
    }

    override fun doInBackground(vararg p0: Void?): com.google.api.services.drive.model.File? {

        Log.i(javaClass.simpleName, "Report Upload Task doInBackground")

        return uploadFile()

    }

    override fun onPostExecute(result: com.google.api.services.drive.model.File?) {
        if (result == null) {
            listener.onFileUploadFail("First time google auth will always be null as per Google Docs and intent is provided for runtime authentication", authIntent!!)
        } else
            listener.onFileUploadSuccess(result)
    }

    private fun getDriveInit(): Drive {
        val transport = AndroidHttp.newCompatibleTransport()
        val jsonFactory = JacksonFactory.getDefaultInstance()
        return Drive.Builder(
                transport, jsonFactory, googleAccountCredential)
                .setApplicationName("com.jouple.posapp")
                .build()
    }

    private fun uploadFile(): com.google.api.services.drive.model.File? {
        Log.i(javaClass.simpleName, "Report Upload Task uploadFile")
        val mService = getDriveInit()

        val metaData = com.google.api.services.drive.model.File()
        metaData.name = file.name
        val mimeType = fileType.toString()
        Log.i("mimeType", mimeType)
        metaData.mimeType = mimeType
        val fileContent = FileContent(mimeType, file)  // if excel file causes issue use text/csv for filecontent constructor

        try {
            Log.i(javaClass.simpleName, "Creating file on drive")
            return mService.files()?.create(metaData, fileContent)
                    ?.setFields("id")
                    ?.execute()
        } catch (e: UserRecoverableAuthIOException) {
            Log.e("google auth", e.toString())
            authIntent = e.intent
            authIntent!!.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        return null
    }
}