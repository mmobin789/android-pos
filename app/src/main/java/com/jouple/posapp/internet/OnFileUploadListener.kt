package com.jouple.posapp.internet

import android.content.Intent
import com.google.api.services.drive.model.File

/**
 * Created by macbook on 05/01/2018.
 */
interface OnFileUploadListener {
    fun onFileUploadSuccess(file: File)
    fun onFileUploadFail(error: String, authIntent: Intent)
}