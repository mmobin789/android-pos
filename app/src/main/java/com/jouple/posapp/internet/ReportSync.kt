package com.jouple.posapp.internet

import android.app.Service
import android.content.Intent
import android.os.Environment
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.drive.DriveScopes
import com.jouple.posapp.api.ApiCalls
import com.jouple.posapp.domain.DateTimeStrategy
import com.jouple.posapp.domain.sale.Admin
import com.jouple.posapp.domain.sale.QuickLoadSale
import com.jouple.posapp.domain.sale.Sale
import com.jouple.posapp.domain.sale.SaleLedger
import com.jouple.posapp.ui.activities.LoginActivity
import com.jouple.posapp.ui.utils.Constant
import com.jouple.posapp.ui.utils.SleekUI
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit


class ReportSync : Service(), OnFileUploadListener {
    private var saleLedger: SaleLedger? = null
    private val recheckDuration: Long = 30
    private var saleList = mutableListOf<Sale>()

    companion object {

        var report: File? = null
    }

    private fun checkAgain() {
        Handler().postDelayed({

            if (isUploadHour())
                createReport()
            else
                checkAgain()  // recurse to check time true to creation false to recursion

        }, TimeUnit.MINUTES.toMillis(recheckDuration))
    }


    override fun onFileUploadSuccess(file: com.google.api.services.drive.model.File) {

        Log.i("GoogleDriveFileCreated", file.id)
        Toast.makeText(this, "Report generated on Google Drive " + file.id, Toast.LENGTH_SHORT).show()
        val reportObj = getTotalSale()
        ApiCalls.uploadReports(reportObj)
        checkAgain()


    }


    private fun getTotalSale(): JSONObject {
        val admin = Constant.getAdmin(this)
        val data = JSONObject()
        data.put("user", admin.name)
        data.put("vendor", admin.vendor)
        data.put("vat", admin.tax)
        data.put("tabName", admin.tab)
        data.put("vatNumber", admin.vatNumber)
        val totalSales = JSONArray()

        val sales = saleLedger!!.allSale
        for (sale in sales) {
            val obj = JSONObject()
            obj.put("invoice_no", sale.id)
            obj.put("startTime", sale.startTime)
            obj.put("endTime", sale.endTime)
            obj.put("status", sale.status)
            val quickLoadSale = sale as QuickLoadSale
            obj.put("Total", quickLoadSale.total)
            obj.put("TotalTax", quickLoadSale.totalTax)
            obj.put("TotalDiscount", quickLoadSale.totalDiscount)
            val items = JSONArray()
            val lineItems = saleLedger!!.getSaleById(sale.id).allLineItem
            for (lineItem in lineItems) {
                val objLineItem = JSONObject()
                objLineItem.put("quantity", lineItem.quantity)
                val productObj = JSONObject()
                val product = lineItem.product
                productObj.put("name", product.name)
                productObj.put("id", product.id)
                productObj.put("cat_id", product.categoryID)
                productObj.put("description", product.description)
                productObj.put("image", product.image)
                productObj.put("barcode", product.barcode)
                productObj.put("unitPrice", product.unitPrice)
                productObj.put("tax", product.tax)
                productObj.put("discount", product.discount)
                objLineItem.put("product", productObj)
                items.put(objLineItem)

            }
            obj.put("items", items)
            totalSales.put(obj)
        }

        data.put("totalSale", totalSales)

        return data


    }

    override fun onFileUploadFail(error: String, authIntent: Intent) {

        val message = "Reports Sync Permission Required."
        // val message = "Report Sync stops here. Google Drive permission required"
        Log.i(javaClass.simpleName, message)
        startActivity(authIntent)
        for (i in 0..1) Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        checkAgain()


    }


    override fun onBind(intent: Intent): IBinder? {

        return null
    }

    private fun isUploadHour(): Boolean {
        val hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        return hour == 13 || hour == 15 || hour == 18


    }


    private fun createReport() {
        if (saleLedger == null) {
            Constant.initiateCoreApp(this)
            saleLedger = SaleLedger.getInstance()
        }
        saleList = saleLedger!!.allSale
        if (saleList.size > 0 && isUploadHour() && !SleekUI.getActiveInternet(this).isNullOrEmpty()) {

            val admin = Constant.getAdmin(this)
            val handlerThread = HandlerThread("Report Creator Thread")
            handlerThread.start()
            val handler = Handler(handlerThread.looper)
            handler.post {
                createExcelReport(admin)

                handlerThread.quit()
            }


        } else {
            if (saleList.size == 0)
                Log.i(javaClass.simpleName, "No reports to create and upload yet will re-check in eta 30 minutes")
            else
                Log.i(javaClass.simpleName, "Hour is not equal to specified and will re-check in eta 30 minutes")
            Handler().postDelayed({
                createReport()
            }, TimeUnit.MINUTES.toMillis(recheckDuration))
        }
    }


    private fun createExcelReport(admin: Admin) {
        // List<LineItem> soldItems = sale.getAllLineItem();
        val workbook = XSSFWorkbook()
        val sheet = workbook.createSheet(DateTimeStrategy.getCurrentTime().substring(0, 10))
        val header = sheet.createRow(0)
        var cell = header.createCell(0)
        cell.setCellValue("Vendor")
        cell = header.createCell(1)
        cell.setCellValue(admin.vendor)
        cell = header.createCell(2)
        cell.setCellValue("User")
        cell = header.createCell(3)
        cell.setCellValue(admin.name)
        val header2 = sheet.createRow(1)
        cell = header2.createCell(0)
        cell.setCellValue("TAB")
        cell = header2.createCell(1)
        cell.setCellValue(admin.tab)
        for (c in 0..3)
            sheet.setColumnWidth(c, 15 * 500)

        var lastSaleItems = 2
        for (i in saleList.indices) {
            val sale = saleList[i]
            val row1 = sheet.createRow(lastSaleItems)
            cell = row1.createCell(0)

            cell.setCellValue("Invoice#")
            cell = row1.createCell(1)
            cell.setCellValue("Date")
            //  cell2.setCellStyle(cs);
            cell = row1.createCell(2)
            //     cell3.setCellStyle(cs);
            cell.setCellValue("Total")
            val row2 = sheet.createRow(row1.rowNum + 1)
            cell = row2.createCell(0)
            cell.setCellValue(sale.id.toString() + "")
            cell = row2.createCell(1)
            cell.setCellValue(sale.endTime)
            cell = row2.createCell(2)
            val quickLoadSale = sale as QuickLoadSale
            cell.setCellValue(quickLoadSale.total.toString())
            val row3 = sheet.createRow(row2.rowNum + 1)
            cell = row3.createCell(0)
            cell.setCellValue("Items")
            cell = row3.createCell(1)
            cell.setCellValue("Quantity")
            cell = row3.createCell(2)
            cell.setCellValue("Price")
            cell = row3.createCell(3)
            cell.setCellValue("VAT")
            val lineItems = saleLedger?.getSaleById(sale.getId())!!.allLineItem
            for (k in lineItems.indices) {
                val lineItem = lineItems.get(k)
                val row4 = sheet.createRow(row3.rowNum + 1 + k)
                val product = lineItem.product
                cell = row4.createCell(0)
                cell.setCellValue(product.name)
                cell = row4.createCell(1)
                cell.setCellValue(lineItem.quantity.toString())
                cell = row4.createCell(2)
                cell.setCellValue((lineItem.priceAtSale!! * lineItem.quantity).toString())
                cell = row4.createCell(3)
                cell.setCellValue((product.tax * lineItem.quantity).toString())
                lastSaleItems = row4.rowNum + 1

            }
        }
        // Create a path where we will place our List of objects on external storage


        val reportsFolder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "MATB reports")
        if (!reportsFolder.exists()) {
            reportsFolder.mkdir()
            Log.i("report", "report Directory created" + reportsFolder.path)

        }
        val child = " MATB Report#" + DateTimeStrategy.getCurrentTime().substring(0, 10)
        val file = File(reportsFolder.absolutePath, child.replace("[^a-zA-Z0-9]+", "") + ".xlsx")
        val os: FileOutputStream

        try {

            os = FileOutputStream(file)
            workbook.write(os)
            Log.w("FileUtils", "Wrote file" + file.path)
            os.close()
            report = file
            uploadReport()

        } catch (e: IOException) {
            Log.e("FileUtils", "Error writing " + file, e)

        } catch (e: Exception) {
            Log.e("FileUtils", "Failed to save file", e)
        }


    }

    private fun uploadReport() {
        Log.i(javaClass.simpleName, "Uploading Report created " + report?.name)

        if (report != null) {
            val googleAuth = GoogleAccountCredential.usingOAuth2(this, listOf(DriveScopes.DRIVE))
            googleAuth.backOff = ExponentialBackOff()
            googleAuth.selectedAccountName = Constant.getPreferences(this).getString(LoginActivity.PREF_ACCOUNT_NAME, null)
            val reportUploadTask = ReportUploadTask(ReportUploadTask.FileType.Excel, googleAuth, report!!, this)
            reportUploadTask.execute()
        } else {
            Log.e(javaClass.simpleName, "Report to Upload is null re-checks in eta 30 minutes")
            createReport()
        }


    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createReport()
        return Service.START_STICKY
    }


}
