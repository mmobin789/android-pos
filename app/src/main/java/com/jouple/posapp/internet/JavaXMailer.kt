//package com.jouple.pos.internet
//
//import android.os.AsyncTask
//import android.util.Log
//import java.util.*
//import javax.mail.*
//import javax.mail.internet.InternetAddress
//import javax.mail.internet.MimeMessage
//
///**
// * Created by macbook on 16/01/2018.
// */
//class JavaXMailer(private val email: String, private val password: String, private val subject: String, private val message: String) : AsyncTask<Void, Void, Boolean>() {
//
//
//    override fun doInBackground(vararg p0: Void?): Boolean {
//
//        //Creating properties
//        val props = Properties()
//
//        //Configuring properties for gmail
//        //If you are not using gmail you may need to change the values
//        props.put("mail.smtp.host", "smtp.gmail.com")
//        props.put("mail.smtp.socketFactory.port", "465")
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
//        props.put("mail.smtp.auth", "true")
//        props.put("mail.smtp.port", "465")
//        //Creating a new session
//        val session = Session.getDefaultInstance(props,
//                object : javax.mail.Authenticator() {
//                    //Authenticating the password
//                    override fun getPasswordAuthentication(): PasswordAuthentication {
//                        return PasswordAuthentication(email, password)
//                    }
//                })
//        try {
//            //Creating MimeMessage object
//            val mm = MimeMessage(session)
//
//            //Setting sender address
//            mm.setFrom(InternetAddress(email))
//            //Adding receiver
//            mm.addRecipient(Message.RecipientType.TO, InternetAddress(email))
//            //Adding subject
//            mm.subject = subject
//            //Adding message
//            mm.setText(message)
//
//            //Sending email
//            Transport.send(mm)
//
//            return true
//
//        } catch (e: MessagingException) {
//            e.printStackTrace()
//
//
//        }
//        return false
//    }
//
//    override fun onPostExecute(result: Boolean?) {
//        Log.i(javaClass.simpleName, result.toString())
//    }
//
//
//}