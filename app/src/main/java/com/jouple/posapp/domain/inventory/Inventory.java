package com.jouple.posapp.domain.inventory;

import android.content.Context;
import android.util.Log;

import com.jouple.posapp.domain.sale.Admin;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.techicalservices.inventory.InventoryDao;
import com.jouple.posapp.ui.utils.Constant;

import static com.jouple.posapp.ui.utils.Constant.getPercentageValue;

/**
 * This class is service locater for Product Catalog and Stock.
 *
 * @author Refresh Team
 */
public class Inventory {

    private static Inventory instance = null;
    private static InventoryDao inventoryDao = null;
    private Stock stock;
    private ProductCatalog productCatalog;

    /**
     * Constructs Data Access Object of inventory.
     *
     * @throws NoDaoSetException if DAO is not exist.
     */
    private Inventory() throws NoDaoSetException {
        if (!isDaoSet()) {
            throw new NoDaoSetException();
        }
        stock = new Stock(inventoryDao);
        productCatalog = new ProductCatalog(inventoryDao);
    }

    public static Product getChargedProduct(Product product, Context context) {


        Category category = inventoryDao.getCategoryById(product.getCategoryID());
        double tax, discount;
        double price = product.getUnitPrice();
        Admin admin = Constant.getAdmin(context);

//        if (product.getTax() > 0) {
//            tax = getPercentageValue(price, product.getTax());
//
//
//        }

        if (product.getDiscount() > 0) {
            discount = getPercentageValue(price, product.getDiscount());

        } else if (category.getDiscount() > 0) {
            discount = getPercentageValue(price, category.getDiscount());

        } else {
            discount = getPercentageValue(price, admin.getDiscount());
        }


//        if (category.getTax() > 0 && product.getTax() <= 0) {
//            tax = getPercentageValue(price, category.getTax());
//
//
//        }


        tax = getPercentageValue(price, admin.getTax());


        // product.setTax(tax);
        // product.setDiscount(discount);
        // product.setUnitPrice(price);
        Log.i("ChargedProduct", product.toString());

        return new Product(product.getId(), product.getName(), product.getImage(), product.getDescription(), category.getId(), product.getBarcode(), price + tax - discount, tax, discount);
    }


    /**
     * Determines whether the DAO already set or not.
     *
     * @return true if the DAO already set; otherwise false.
     */
    public static boolean isDaoSet() {
        return inventoryDao != null;
    }

    /**
     * Sets the database connector.
     *
     * @param dao Data Access Object of inventory.
     */
    public static void setInventoryDao(InventoryDao dao) {
        inventoryDao = dao;
    }

    /**
     * Returns the instance of this singleton class.
     *
     * @return instance of this class.
     * @throws NoDaoSetException if DAO was not set.
     */
    public static Inventory getInstance() throws NoDaoSetException {
        if (instance == null)
            instance = new Inventory();
        return instance;
    }

    /**
     * Returns product catalog using in this inventory.
     *
     * @return product catalog using in this inventory.
     */
    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    /**
     * Returns stock using in this inventory.
     *
     * @return stock using in this inventory.
     */
    public Stock getStock() {
        return stock;
    }

}
