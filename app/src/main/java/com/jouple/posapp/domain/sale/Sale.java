package com.jouple.posapp.domain.sale;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sale represents sale operation.
 *
 * @author Refresh Team
 */
public class Sale {
    @SerializedName("invoice_no")
    private final int id;
    @Expose
    private String startTime;
    @Expose
    private String endTime;
    @Expose
    private String status;
    @Expose
    private List<LineItem> items;


    public Sale(int id, String startTime) {
        this(id, startTime, startTime, "", new ArrayList<LineItem>());
    }

    /**
     * Constructs a new Sale.
     *
     * @param id        ID of this Sale.
     * @param startTime start time of this Sale.
     * @param endTime   end time of this Sale.
     * @param status    status of this Sale.
     * @param items     list of LineItem in this Sale.
     */
    public Sale(int id, String startTime, String endTime, String status, List<LineItem> items) {
        this.id = id;
        this.startTime = startTime;
        this.status = status;
        this.endTime = endTime;
        this.items = items;
    }

    /**
     * Returns list of LineItem in this Sale.
     *
     * @return list of LineItem in this Sale.
     */
    public List<LineItem> getAllLineItem() {
        return items;
    }

    /**
     * Add Product to Sale.
     *
     * @param product  product to be added.
     * @param quantity quantity of product that added.
     * @return LineItem of Sale that just added.
     */
    LineItem addLineItem(Product product, int quantity) {

        for (LineItem lineItem : items) {
            if (lineItem.getProduct().getId() == product.getId()) {
                lineItem.addQuantity(quantity);
                return lineItem;
            }
        }

        LineItem lineItem = new LineItem(product, quantity);
        items.add(lineItem);
        return lineItem;
    }

    public int size() {
        return items.size();
    }

    /**
     * Returns a LineItem with specific index.
     *
     * @param index of specific LineItem.
     * @return a LineItem with specific index.
     */
    public LineItem getLineItemAt(int index) {
        if (index >= 0 && index < items.size())
            return items.get(index);
        return null;
    }

    /**
     * Returns the total price of this Sale.
     *
     * @return the total price of this Sale.
     */
    public double getTotal() {
        double amount = 0;
        for (LineItem lineItem : items) {
            amount += lineItem.getTotalPriceAtSale();
        }
        return amount;
    }

//    public double getSubTotal() {
//        double d = 0;
//        for (LineItem lineItem : items) {
//            d += (((lineItem.getProduct().getUnitPrice() + lineItem.getProduct().getDiscount()) - lineItem.getProduct().getTax()) * lineItem.getQuantity());
//        }
//        return d;
//    }

    public double getTotalTax() {
        double tax = 0;
        for (LineItem lineItem : items) {
            tax += lineItem.getProduct().getTax() * lineItem.getQuantity();
        }
        return tax;
    }

    public double getTotalDiscount() {
        double discount = 0;
        for (LineItem lineItem : items) {
            discount += lineItem.getProduct().getDiscount() * lineItem.getQuantity();
        }
        return discount;
    }

    public int getId() {
        return id;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStatus() {
        return status;
    }

    /**
     * Returns the total quantity of this Sale.
     *
     * @return the total quantity of this Sale.
     */
    public int getOrders() {
        int orderCount = 0;
        for (LineItem lineItem : items) {
            orderCount += lineItem.getQuantity();
        }
        return orderCount;
    }

    /**
     * Returns the description of this Sale in Map format.
     *
     * @return the description of this Sale in Map format.
     */
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id + "");
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("status", getStatus());
        map.put("total", getTotal() + "");
        map.put("orders", getOrders() + "");

        return map;
    }

    /**
     * Removes LineItem from Sale.
     *
     * @param lineItem lineItem to be removed.
     */
    public void removeItem(LineItem lineItem) {
        items.remove(lineItem);
    }

}