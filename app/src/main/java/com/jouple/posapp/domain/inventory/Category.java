package com.jouple.posapp.domain.inventory;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.jouple.posapp.techicalservices.NoDaoSetException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 06/11/2017.
 */

public class Category implements Parent<Product> {
    private String name;
    private int id;
    private double tax, discount;

    public Category(int id) {
        this.id = id;
    }

    public Category(int id, String name, double tax, double discount) {
        this(id);
        this.name = name;
        this.tax = tax;
        this.discount = discount;
    }

    public Category(String name, double tax, double discount) {
        this(-1, name, tax, discount);
    }

    public Category(String name) {
        this(-1);
        this.name = name;

    }

    public int getId() {
        return id;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<Product> getChildList() {
        try {
            List<Product> db = Inventory.getInstance().getProductCatalog().getProductsByCategoryID(id);
            List<Product> products = new ArrayList<>();

            for (Product product : db) {
                if (!product.getName().equalsIgnoreCase("misc"))
                    products.add(product);
            }
            return products;
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        return new ArrayList<>(0);
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
