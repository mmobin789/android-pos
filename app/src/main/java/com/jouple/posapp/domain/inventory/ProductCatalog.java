package com.jouple.posapp.domain.inventory;

import android.text.TextUtils;

import com.jouple.posapp.techicalservices.inventory.InventoryDao;

import java.util.List;

/**
 * Book that keeps list of Product.
 *
 * @author Refresh Team
 */
public class ProductCatalog {

    private InventoryDao inventoryDao;

    /**
     * Constructs Data Access Object of inventory in ProductCatalog.
     *
     * @param inventoryDao DAO of inventory.
     */
    public ProductCatalog(InventoryDao inventoryDao) {
        this.inventoryDao = inventoryDao;
    }

    /**
     * Constructs product and adds product to inventory.
     *
     * @param name             name of product.
     * @param productImagePath
     * @param barcode          barcode of product.
     * @param salePrice        price of product.   @return true if product adds in inventory success ; otherwise false.
     */
    public boolean addProduct(String name, String productImagePath, String desc, int categoryID, String barcode, double salePrice, double tax, double discount) {
        if (TextUtils.isEmpty(productImagePath))
            productImagePath = "no image";
        Product product = new Product(name, productImagePath, desc, categoryID, barcode, salePrice, tax, discount);
        int id = inventoryDao.addProduct(product);
        return id != -1;
    }


    public int addProduct(String name, String productImagePath, String desc, int categoryID, String barcode, double salePrice) {
        if (TextUtils.isEmpty(productImagePath))
            productImagePath = "no image";
        Product product = new Product(name, productImagePath, desc, categoryID, barcode, salePrice, 0, 0);
        return inventoryDao.addProduct(product);
    }

    public boolean addCategory(String name, double tax, double discount) {
        Category category = new Category(name, tax, discount);
        int id = inventoryDao.addCategory(category);
        return id != -1;

    }

    public String getProductsQuantityInCategory(int catID) {

        return getProductsByCategoryID(catID).size() + "";
    }

    /**
     * Edits product.
     *
     * @param product the product to be edited.
     * @return true if product edits success ; otherwise false.
     */
    public boolean editProduct(Product product) {
        return inventoryDao.editProduct(product);
    }

    /**
     * Returns product from inventory finds by barcode.
     *
     * @param barcode barcode of product.
     * @return product
     */
    public Product getProductByBarcode(String barcode) {
        return inventoryDao.getProductByBarcode(barcode);
    }


    public List<Product> getProductsByCategoryID(int categoryID) {
        return inventoryDao.getProductsByCategoryId(categoryID);
    }

    public Category getCategoryByID(int id) {
        return inventoryDao.getCategoryById(id);
    }

    /**
     * Returns product from inventory finds by id.
     *
     * @param id id of product.
     * @return product
     */
    public Product getProductById(int id) {
        return inventoryDao.getProductById(id);
    }

    /**
     * Returns list of all products in inventory.
     *
     * @return list of all products in inventory.
     */
    public List<Product> getAllProduct() {
        return inventoryDao.getAllProduct();
    }

    public List<Category> getAllCategories() {
        return inventoryDao.getAllCategories();
    }

    /**
     * Returns list of product in inventory finds by name.
     *
     * @param name name of product.
     * @return list of product in inventory finds by name.
     */
    public List<Product> getProductByName(String name) {
        return inventoryDao.getProductByName(name);
    }

    /**
     * Search product from string in inventory.
     *
     * @param search string for searching.
     * @return list of product.
     */
    public List<Product> searchProduct(String search) {
        return inventoryDao.searchProduct(search);
    }

    public List<Category> searchCategory(String search) {
        return inventoryDao.searchCategory(search);
    }

    /**
     * Clears ProductCatalog.
     */
    public void clearProductCatalog() {
        inventoryDao.clearProductCatalog();
    }

    /**
     * Hidden product from inventory.
     *
     * @param product The product to be hidden.
     */
    public void suspendProduct(Product product) {
        inventoryDao.suspendProduct(product);
    }

    public boolean removeCategory(Category category) {
        return inventoryDao.removeCategory(category);
    }

    public boolean removeProduct(Product product) {
        return inventoryDao.removeProduct(product);
    }

    public boolean editCategory(Category category) {
        return inventoryDao.editCategory(category);
    }


}
