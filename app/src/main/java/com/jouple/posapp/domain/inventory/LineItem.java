package com.jouple.posapp.domain.inventory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * LineItem of Sale.
 *
 * @author Refresh Team
 */
public class LineItem implements Parcelable {

    /**
     * Static value for UNDEFINED ID.
     */
    public static final int UNDEFINED = -1;
    public static final Parcelable.Creator<LineItem> CREATOR = new Parcelable.Creator<LineItem>() {

        @Override
        public LineItem createFromParcel(Parcel parcel) {
            return new LineItem(parcel);
        }

        @Override
        public LineItem[] newArray(int i) {
            return new LineItem[i];
        }
    };
    @SerializedName("product")
    private Product product;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("id")
    private int id;
    @SerializedName("salePrice")
    private double unitPriceAtSale;

    /**
     * Constructs a new LineItem.
     *
     * @param product  product of this LineItem.
     * @param quantity product quantity of this LineItem.
     */
    public LineItem(Product product, int quantity) {
        this(UNDEFINED, product, quantity, product.getUnitPrice());
    }


    /**
     * Constructs a new LineItem.
     *
     * @param id              ID of this LineItem, This value should be assigned from database.
     * @param product         product of this LineItem.
     * @param quantity        product quantity of this LineItem.
     * @param unitPriceAtSale unit price at sale time. default is price from ProductCatalog.
     */
    public LineItem(int id, Product product, int quantity,
                    double unitPriceAtSale) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
        this.unitPriceAtSale = unitPriceAtSale;
    }

    private LineItem(Parcel parcel) {
        id = parcel.readInt();
        quantity = parcel.readInt();
        unitPriceAtSale = parcel.readDouble();
        product = parcel.readParcelable(Product.class.getClassLoader());
    }

    /**
     * Returns product in this LineItem.
     *
     * @return product in this LineItem.
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Return quantity of product in this LineItem.
     *
     * @return quantity of product in this LineItem.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity of product in this LineItem.
     *
     * @param quantity quantity of product in this LineItem.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Adds quantity of product in this LineItem.
     *
     * @param amount amount for add in quantity.
     */
    public void addQuantity(int amount) {
        this.quantity += amount;
    }

    /**
     * Returns total price of this LineItem.
     *
     * @return total price of this LineItem.
     */
    public double getTotalPriceAtSale() {
        return unitPriceAtSale * quantity;
    }

    /**
     * Returns the description of this LineItem in Map format.
     *
     * @return the description of this LineItem in Map format.
     */
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", product.getName());
        map.put("quantity", quantity + "");
        map.put("price", getTotalPriceAtSale() + "");
        return map;

    }

    /**
     * Returns id of this LineItem.
     *
     * @return id of this LineItem.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id of this LineItem.
     *
     * @param id of this LineItem.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets price product of this LineItem.
     *
     * @param unitPriceAtSale price product of this LineItem.
     */
    public void setUnitPriceAtSale(double unitPriceAtSale) {
        this.unitPriceAtSale = unitPriceAtSale;
    }

    /**
     * Returns price product of this LineItem.
     *
     * @return unitPriceAtSale price product of this LineItem.
     */
    public Double getPriceAtSale() {
        return unitPriceAtSale;
    }

    /**
     * Determines whether two objects are equal or not.
     *
     * @return true if Object is a LineItem with same ID ; otherwise false.
     */
    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;
        if (!(object instanceof LineItem))
            return false;
        LineItem lineItem = (LineItem) object;
        return lineItem.getId() == this.getId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(quantity);
        parcel.writeDouble(unitPriceAtSale);
        parcel.writeParcelable(product, i);
    }
}
