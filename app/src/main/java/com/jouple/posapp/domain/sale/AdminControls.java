package com.jouple.posapp.domain.sale;

import android.content.ContentValues;

import com.jouple.posapp.techicalservices.Database;
import com.jouple.posapp.techicalservices.DatabaseContents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 21/11/2017.
 */

public class AdminControls implements AdminDAO {
    private static AdminControls adminControls;
    private Database database;

    private AdminControls() {
    }

    public AdminControls(Database database) {
        this.database = database;
    }

    public static AdminControls getInstance() {
        if (adminControls == null)
            adminControls = new AdminControls();
        return adminControls;
    }

    private List<Admin> getAllAdmins(String condition) {
        String queryString = "SELECT * FROM " + DatabaseContents.TABLE_ADMIN.toString() + condition + " ORDER BY name";
        return toAdminsList(database.select(queryString));
    }

    /**
     * Converts list of object to list of admins.
     *
     * @param objectList list of object.
     * @return list of admins.
     */
    private List<Admin> toAdminsList(List<Object> objectList) {
        List<Admin> list = new ArrayList<>();
        for (Object object : objectList) {
            ContentValues content = (ContentValues) object;
            Admin admin = new Admin(content.getAsString("name"),
                    content.getAsString("password"), content.getAsString("vendor"), content.getAsInteger("_id"));
            admin.setTax(content.getAsDouble("tax"));
            list.add(admin);

        }
        return list;
    }

    @Override
    public int addAdmin(Admin admin) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("tax", admin.getTax());
        contentValues.put("name", admin.getName());
        contentValues.put("password", admin.getPassword());
        return database.insert(DatabaseContents.TABLE_ADMIN.toString(), contentValues); // returns id of row when inserted
    }

    @Override
    public Admin getAdmin(int id) {
        List<Admin> admins = getAdminBy("_id", id + "");
        if (admins.size() > 0)
            return admins.get(0);
        else return null;
    }

    private List<Admin> getAdminBy(String reference, String value) {
        String condition = " WHERE " + reference + " = " + value + " ;";
        String queryString = "SELECT DISTINCT _id,tax,password FROM " + DatabaseContents.TABLE_ADMIN.toString() + condition + " ORDER BY name";

        return getAllAdmins(queryString);
    }

    @Override
    public List<Admin> getAllAdmins() {
        return null;
    }
//    @Override
//    public boolean editTax(double tax) {
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("tax", tax);
//        return database.update(DatabaseContents.TABLE_ADMIN.toString(), contentValues);

}
