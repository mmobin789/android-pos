package com.jouple.posapp.domain.inventory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Product or item represents the real product in store.
 *
 * @author Refresh Team
 */
public class Product implements Parcelable {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    /**
     * Static value for UNDEFINED ID.
     */
    private static final int UNDEFINED_ID = -1;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;
    @SerializedName("cat_id")
    private int categoryID = -1;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;
    @SerializedName("barcode")
    private String barcode;
    @SerializedName("unitPrice")
    private double unitPrice;
    @SerializedName("tax")
    private double tax;
    @SerializedName("discount")
    private double discount;

    /**
     * Constructs a new Product.
     *
     * @param id        ID of the product, This value should be assigned from database.
     * @param name      name of this product.
     * @param barcode   barcode (any standard format) of this product.
     * @param salePrice price for using when doing sale.
     */
    public Product(int id, String name, String image, String description, int categoryID, String barcode, double salePrice, double tax, double discount) {
        this.id = id;
        this.name = name;
        this.barcode = barcode;

        this.unitPrice = salePrice;
        this.categoryID = categoryID;
        this.tax = tax;
        this.discount = discount;
        this.image = image;
        this.description = description;

    }

    public Product(String name, String image, String description, int categoryID, String barcode, double salePrice, double tax, double discount) {
        this(UNDEFINED_ID, name, image, description, categoryID, barcode, salePrice, tax, discount);
    }

    private Product(Parcel parcel) {
        name = parcel.readString();
        image = parcel.readString();
        barcode = parcel.readString();
        unitPrice = parcel.readDouble();
        categoryID = parcel.readInt();
        id = parcel.readInt();
        tax = parcel.readDouble();
        discount = parcel.readDouble();
    }

    @Override
    public String toString() {
        return "Product{" +
                "description='" + description + '\'' +
                ", id=" + id +
                ", categoryID=" + categoryID +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", barcode='" + barcode + '\'' +
                ", unitPrice=" + unitPrice +
                ", tax=" + tax +
                ", discount=" + discount +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {

        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public double getTax() {
        return tax;
    }

    /**
     * Returns category of this product.
     *
     * @return category of this product.
     */
//    public Category getCategory() {
//        return category;
//    }
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * Sets category of this product.
     */
//    public void setCategory(Category category) {
//        this.category = category;
//    }
    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * Returns name of this product.
     *
     * @return name of this product.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of this product.
     *
     * @param name name of this product.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns id of this product.
     *
     * @return id of this product.
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns barcode of this product.
     *
     * @return barcode of this product.
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Sets barcode of this product.
     *
     * @param barcode barcode of this product.
     */
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    /**
     * Returns price of this product.
     *
     * @return price of this product.
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets price of this product.
     *
     * @param unitPrice price of this product.
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Returns the description of this Product in Map format.
     *
     * @return the description of this Product in Map format.
     */
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id + "");
        map.put("name", name);
        map.put("barcode", barcode);
        map.put("unitPrice", unitPrice + "");
        map.put("cat_id", categoryID + "");
        map.put("product_tax", tax + "");
        map.put("product_discount", discount + "");
        return map;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(image);
        parcel.writeString(barcode);
        parcel.writeDouble(unitPrice);
        parcel.writeInt(categoryID);
        parcel.writeInt(id);
        parcel.writeDouble(tax);
        parcel.writeDouble(discount);
    }


}
