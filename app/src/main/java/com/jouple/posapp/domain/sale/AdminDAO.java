package com.jouple.posapp.domain.sale;

import java.util.List;

/**
 * Created by macbook on 21/11/2017.
 */

public interface AdminDAO {

    int addAdmin(Admin admin);

    Admin getAdmin(int id);

    List<Admin> getAllAdmins();
}
