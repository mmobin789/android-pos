package com.jouple.posapp.domain.sale;

/**
 * Created by macbook on 17/11/2017.
 */

public class Admin {
    private static AdminDAO adminDAO;
    private String name;
    private String tab;
    private String password, vendor;
    private double tax, discount;
    private int vatNumber;
    private int id;

    public Admin(String name, String password, String vendor, int id) {
        this.name = name;
        this.password = password;
        this.id = id;
        this.vendor = vendor;

    }

    public Admin(String name, String vendor, String password) {
        this(name, password, vendor, -1);
    }

    public static Admin getAdminSettings() {
        return new Admin(null, null, null);
    }

    public static void setAdminDAO(AdminDAO dao) {
        adminDAO = dao;

    }

    public int getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(int vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getId() {
        return id;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
