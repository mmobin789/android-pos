package com.jouple.posapp.ui.inventory;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import com.jouple.posapp.R;
import com.jouple.posapp.ui.component.OnFragmentViewListener;
import com.jouple.posapp.ui.component.PagerAdapter;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.sale.SaleFragment;
import com.jouple.posapp.ui.utils.AppTextView;
import com.jouple.posapp.ui.utils.OrientationUtils;
import com.jouple.posapp.ui.utils.SleekUI;
import com.wang.avi.AVLoadingIndicatorView;

import static com.jouple.posapp.ui.activities.MainActivity.showInventoryUIOnly;
import static com.jouple.posapp.ui.utils.Constant.isAdmin;

/**
 * Created by macbook on 05/12/2017.
 */

public class MainUI extends UpdatableFragment implements ViewPager.OnPageChangeListener {

    public static String searchTxt = "";
    private static Dialog dialog;
    public PagerAdapter adapter;
    public View catView;
    public ViewPager inventory;
    SaleFragment saleFragment;
    AppTextView add;
    RelativeLayout root;


    public MainUI() {

    }

    @Nullable
    public static Dialog getLoaderDialog() {
        return dialog;
    }
    public static Dialog loaderDialog(Context context) {


        dialog = new Dialog(context);
        assert dialog.getWindow() != null;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_layout);
        AVLoadingIndicatorView avLoadingIndicatorView;
        avLoadingIndicatorView = dialog.findViewById(R.id.av);
        avLoadingIndicatorView.show();


        return dialog;
    }

    //    public void showBlurView() {
//        Blurry.with(getContext()).radius(15).sampling(2).onto(root);
//        avLoadingIndicatorView.show();
//    }
//
//    public void hideBlurView() {
//        Blurry.delete(root);
//        avLoadingIndicatorView.hide();
//    }
    public static void refresh(MainUI mainUI)

    {
        assert mainUI.getFragmentManager() != null;
        mainUI.getFragmentManager().beginTransaction().detach(mainUI).attach(mainUI).commit();
    }

    private void showMenu(final View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v);
        Menu menu = popup.getMenu();
        popup.getMenuInflater().inflate(R.menu.cart, menu);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getOrder()) {
                    case 0:
                        ((CategoryFragment) adapter.getItem(0)).addCategoryDialog(v);
                        break;
                    case 1:
                        ((ProductsFragment) adapter.getItem(1)).showPopup(getChildFragmentManager());
                        break;

                }
                return true;
            }
        });

//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
    }

    private void checkAdmin() {
        int i;
        if (isAdmin) {

            i = View.VISIBLE;

        } else {
            i = View.GONE;

        }


        add.setVisibility(i);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OrientationUtils.lockOrientation(getMainActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.new_ui, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        saleFragment = (SaleFragment) getChildFragmentManager().findFragmentById(R.id.salesFragment);
        PagerAdapter.saleFragment = saleFragment;
        if (showInventoryUIOnly) {
            getChildFragmentManager().beginTransaction().hide(saleFragment).commit();
            showInventoryUIOnly = false;
            refresh(this);

        }


        root = view.findViewById(R.id.root);


        update();

        inventory = view.findViewById(R.id.pager);
        adapter = new PagerAdapter(getChildFragmentManager());
        inventory.setAdapter(adapter);
        add = view.findViewById(R.id.add);
        add.setTypeface(SleekUI.fontAwesome(getContext()));
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });
        checkAdmin();
        adapter.setOnFragmentViewListener(new OnFragmentViewListener() {
            @Override
            public void OnFragmentViewNonNull(View view) {
                catView = view;
            }
        });
        inventory.addOnPageChangeListener(this);


    }

    @Override
    public void update() {
        assert saleFragment.getView() != null;
        if (getMainActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            saleFragment.getView().findViewById(R.id.image).setVisibility(View.GONE);
        } else {
            saleFragment.getView().findViewById(R.id.image).setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1)
            ProductsFragment.getInstance().searchBox.setText(searchTxt);
        else
            CategoryFragment.getInstance().searchBox.setText("");
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
