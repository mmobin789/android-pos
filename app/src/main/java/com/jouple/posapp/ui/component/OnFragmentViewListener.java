package com.jouple.posapp.ui.component;

import android.view.View;

/**
 * Created by macbook on 06/12/2017.
 */

public interface OnFragmentViewListener {
    void OnFragmentViewNonNull(View view);
}
