package com.jouple.posapp.ui.activities;

import android.content.Intent;
import android.devkit.api.Misc;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.DateTimeStrategy;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.component.SalesAdapter;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.PrinterReaderThread;
import com.jouple.posapp.ui.utils.SleekUI;

import org.apache.poi.util.StringUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class PaymentDetailActivity extends BaseActivity {
    EditText input;
    List<LineItem> lineItems;
    File invoicePDF;
    String totalPrice, subTotal, change, remaining;
    String tax;
    String discount;
    RelativeLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Misc.printerEnable(true);
        setContentView(R.layout.activity_payment);
        initUI();
        //  Toast.makeText(this, "Your chosen payment method was " + Constant.paymentType, Toast.LENGTH_SHORT).show();
        // removeMisc();

    }

    private void removeMisc() {
        Product misc = getIntent().getParcelableExtra("misc");
        if (misc != null) {
            try {

                Inventory.getInstance().getProductCatalog().removeProduct(misc);
            } catch (NoDaoSetException e) {
                e.printStackTrace();
            }
        }
    }

    private void initUI() {
        SleekUI.getLabelView(this, R.id.the).setTypeface(SleekUI.logoFont(1, this));
        SleekUI.getLabelView(this, R.id.market).setTypeface(SleekUI.logoFont(0, this));
        SleekUI.getLabelView(this, R.id.beach).setTypeface(SleekUI.logoFont(2, this));
        root = findViewById(R.id.root);
        findViewById(R.id.tax).setVisibility(View.GONE);
        findViewById(R.id.image).setVisibility(View.GONE);
        findViewById(R.id.categoryName).setVisibility(View.GONE);
        // findViewById(R.id.title).setVisibility(View.VISIBLE);
        RecyclerView rv = findViewById(R.id.saleListView);
        SleekUI.initRecyclerView(rv, true);
//        SleekUI.setHeader("Invoice", this);
        lineItems = getIntent().getParcelableArrayListExtra("lineItems");
        SalesAdapter adapter = new SalesAdapter(lineItems);
        adapter.setPrintingUI(true);
        rv.setAdapter(adapter);
        totalPrice = getIntent().getStringExtra("tp");
        tax = getIntent().getStringExtra("tax");
        discount = getIntent().getStringExtra("discount");
        remaining = getIntent().getStringExtra("remaining");
        //  String tax = getIntent().getStringExtra("tax");
        //   String discount = getIntent().getStringExtra("discount");
        input = findViewById(R.id.dialog_saleInput);
        TextView totalPriceTV = findViewById(R.id.payment_total);
        TextView subTotalPriceTV = findViewById(R.id.subtotal);
        subTotal = getIntent().getStringExtra("price");
        change = getIntent().getStringExtra("change");
        subTotalPriceTV.setText(subTotal);
        //  TextView taxTV = findViewById(R.id.tax);
        //    TextView discountTV = findViewById(R.id.discount);
        totalPriceTV.setText(totalPrice);
        //taxTV.setText(tax);
        //discountTV.setText(discount);
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        findViewById(R.id.email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(view);
            }
        });
        findViewById(R.id.print).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //print(view);
                //   printUsingSystem();
            }
        });
        runInBackground();
        printUsingSystem();
    }

    private String getData()

    {

        StringBuilder stringBuilder = new StringBuilder(lineItems.size());
        for (LineItem lineItem : lineItems) {


            stringBuilder.append(lineItem.getQuantity()).append(" ").append(lineItem.getProduct().getName()).append(" ")
                    .append(Constant.decimalPlaces(lineItem.getProduct().getUnitPrice() * lineItem.getQuantity())).append("\n\n");


        }

        return stringBuilder.toString();
    }

    @NonNull
    private String getInvoice() {
        String orderNo = getIntent().getIntExtra("order", -1) + " #" + DateTimeStrategy.getCurrentTime();
        return "The Market At The Beach UAE" + "\n\n" +
                "Invoice No." + orderNo.split("#")[0] + "\n\n" +
                "Invoice Date " + DateTimeStrategy.getCurrentTime().substring(0, 10) + "\n\n" +
                "Order NO." + orderNo + "\n\n" + "QTY ITEM TOTAL" + "\n\n" +
                getData() + "Total(incl VAT) " + totalPrice + "\n\n" + "Amount Received " + remaining + "\n\n" + "Change " + change
                + "\n\n" + "VAT % Type " + Constant.decimalPlaces(Constant.getAdmin(this).getTax()) + "\n\n" + "Total VAT collected" + " " + tax
                + "\n\n" + "VAT number " + "\n\n" + Constant.getAdmin(this).getVatNumber() + "\n\n" + Constant.paymentType + " " + totalPrice + "\n\n\n";
    }

    private void printUsingSystem() {


        try {
            byte[] printerData = getInvoice().getBytes(StringUtil.getPreferredEncoding());
            Log.i("printerData", new String(printerData));
            PrinterReaderThread printerReaderThread = new PrinterReaderThread();
            printerReaderThread.start();
            printerReaderThread.sendCommand(printerData);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        Misc.printerEnable(false);
        super.onDestroy();

    }

    private void runInBackground() {
        final HandlerThread handlerThread = new HandlerThread("PDF Creator Thread");
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                createInvoicePDF();
                handlerThread.quit();
            }
        });
    }
//
//    private void print(View v) {
//        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
//        boolean printerAvailable = btAdapter != null && btAdapter.getBondedDevices() != null && btAdapter.getBondedDevices().size() > 0;
//        if (printerAvailable) {
//            final BluetoothDevice mBtDevice = btAdapter.getBondedDevices().iterator().next();   // Get first paired device
//
//            final BluetoothPrinter printer = new BluetoothPrinter(mBtDevice);
//            printer.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
//
//                @Override
//                public void onConnected() {
//
//                    String s = printer.getDevice().getName();
//                    printer.setAlign(BluetoothPrinter.ALIGN_CENTER);
//                    printer.printText("Hello World!");
//                    printer.addNewLine();
//
//                    printer.finish();
//
//                    Toast.makeText(PaymentDetailActivity.this, s, Toast.LENGTH_SHORT).show();
//                }
//
//                @Override
//                public void onFailed(String error) {
//
//                    Toast.makeText(PaymentDetailActivity.this, R.string.no_printer, Toast.LENGTH_SHORT).show();
//                    Log.d("BluetoothPrinter", "Conection failed");
//                }
//
//            });
//        } else {
//            Toast.makeText(this, R.string.no_printer, Toast.LENGTH_SHORT).show();
//        }
//
//    }

    private void viewPdf() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(invoicePDF), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void createInvoicePDF() {
        File parent;
//        if (isExternalStorageWritable()) {
//            parent = Environment.getExternalStorageDirectory();
//        } else {
        parent = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        File pdfFolder = new File(parent, getString(R.string.app_name) + " invoices");
        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i("pdf", "Invoices Directory created" + pdfFolder.getPath());

        }
        String child = getString(R.string.app_name) + " Invoice # " + getIntent().getIntExtra("order", -1);
        invoicePDF = new File(pdfFolder.getAbsolutePath(), child.replaceAll("[^a-zA-Z0-9]+", "") + ".pdf");

        Log.i("InvoicePDF created", invoicePDF.getPath());
        Constant.saveLastInvoice(this, invoicePDF.getAbsolutePath());

        try {
            FileOutputStream outputStream = new FileOutputStream(invoicePDF);


//            Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
//            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            Document document = new Document();
            PdfWriter.getInstance(document, outputStream);
            document.open();
            addMetaData(document);
            addContent(document);
            document.close();

        } catch (IOException e) {
            e.printStackTrace();
            //    PrinterReaderThread.Companion.sendMail(Log.getStackTraceString(e), true);

        } catch (DocumentException e) {
            e.printStackTrace();
            //  PrinterReaderThread.Companion.sendMail(Log.getStackTraceString(e), true);
        }

        //PrinterReaderThread.Companion.sendMail("InvoicePDF created " + invoicePDF.getPath(), false);

    }


    private Image addImage(@DrawableRes int drawableID) throws IOException, BadElementException {
        Drawable d = ContextCompat.getDrawable(this, drawableID);

        BitmapDrawable bitDw = ((BitmapDrawable) d);

        Bitmap bmp = bitDw.getBitmap();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return Image.getInstance(stream.toByteArray());

    }

    /**
     * In this method the main contents of the documents are added
     *
     * @param document
     * @throws DocumentException
     */


    private void addContent(Document document) throws DocumentException, IOException {

        Image image = addImage(R.drawable.logo);
        image.scaleAbsolute(100, 100);
        image.setAlignment(Element.ALIGN_CENTER);
        image.setBorder(Rectangle.BOX);
        image.setBorderColor(BaseColor.BLACK);
        document.add(image);

        Paragraph reportBody = new Paragraph();
        addEmptyLine(reportBody, 3);
        reportBody.setFont(getBoldFont()); //public static Font FONT_BODY = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.NORMAL);

        // Creating a table
        createTable(reportBody);

        // add extras
        addEmptyLine(reportBody, 2);
        String end = getString(R.string.app_name) + " UAE All rights reserved.";


        Paragraph bill = new Paragraph("Total \t\t " + totalPrice + "" + "\n" + "SubTotal \t\t " + subTotal + "" + "\n"
                //  + "Tax \t\t " + tax + "\n" +
                + "Discount \t\t " + discount
        );
        bill.setAlignment(Element.ALIGN_CENTER);
        reportBody.add(bill);
        addEmptyLine(reportBody, 5);
        bill = new Paragraph(end);
        bill.setAlignment(Element.ALIGN_CENTER);
        reportBody.add(bill);


        // now add all this to the document
        document.add(reportBody);

    }

    /**
     * This method is used to add empty lines in the document
     *
     * @param paragraph
     * @param number
     */
    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Chunk(" "));
        }
    }

    Font getContentFont() {
        return new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
    }

    Font getBoldFont() {
        return new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    }

    private void addCell(String content, PdfPTable table, boolean isHeader, boolean border) {
        Font f;

        if (isHeader)
            f = getBoldFont();
        else
            f = getContentFont();
        PdfPCell cell = new PdfPCell(new Phrase(content, f)); //Public static Font FONT_TABLE_HEADER = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
        if (!border) {


            cell.setBorder(Rectangle.NO_BORDER);
        }
        //cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER); //alignment

        // cell.setBackgroundColor(new Whi); //cell background color
        //    cell.setFixedHeight(30); //cell height
        table.addCell(cell);
    }


    private void createTable(Paragraph reportBody) {
        //float[] columnWidths = {10, 10, 10, 10, 10};
        PdfPTable table = new PdfPTable(5);
        table.setTotalWidth(255);
        table.setLockedWidth(true);
        String[] cellNames = {"Product", "Quantity", "Tax", "Discount", "Price"};

        for (int i = 0; i < 5; i++) {       //Adding table headers
            addCell(cellNames[i], table, true, true);
        }

        for (int i = 0; i < lineItems.size(); i++) {
            LineItem l = lineItems.get(i);
//            product = product.concat(l.getProduct().getName());
//            taxT = taxT.concat(l.getProduct().getTax() + "");
//            quantity = quantity.concat(l.getQuantity() + "");
//            discountD = discountD.concat(l.getProduct().getDiscount() + "");
//            price = price.concat(l.getProduct().getUnitPrice() + "");
            addCell(l.getProduct().getName(), table, false, false);
            addCell(l.getQuantity() + "", table, false, false);
            addCell(l.getProduct().getTax() + "", table, false, false);
            addCell(l.getProduct().getDiscount() + "", table, false, false);
            addCell(l.getProduct().getUnitPrice() + "", table, false, false);

        }
        reportBody.add(table);

    }


    /**
     * iText allows to add metadata to the PDF which can be viewed in your Adobe Reader. If you right click
     * on the file and to to properties then you can see all these information.
     *
     * @param document
     */
    private void addMetaData(Document document) {
        document.addTitle("Invoice");
        document.addSubject("none");
        //  document.addKeywords("Java, PDF, iText");
        document.addAuthor("MATB");
        document.addCreator("MATB");
    }

    private void sendEmail(View v) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        //   email.setType("text/plain");
        email.putExtra(Intent.EXTRA_SUBJECT, invoicePDF.getName());
        // email.putExtra(Intent.EXTRA_TEXT, mBodyEditText.getText().toString());
        Uri fileUri = FileProvider.getUriForFile(this, getPackageName() + ".ui.utils.PosFileProvider", invoicePDF);
        email.putExtra(Intent.EXTRA_STREAM, fileUri);
        //  email.setType("message/rfc822");
        email.setType("vnd.android.cursor.dir/email");
        startActivity(email);

    }


}
