package com.jouple.posapp.ui.component;

import android.view.View;
import android.view.ViewGroup;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Category;
import com.jouple.posapp.domain.inventory.ProductCatalog;

import java.util.List;

/**
 * Created by macbook on 06/11/2017.
 */

public class CategoryAdapter extends BaseRecyclerAdapter {
    private List<Category> list;
    private ProductCatalog productCatalog;


    public CategoryAdapter(ProductCatalog productCatalog, List<Category> list) {
        this.list = list;
        this.productCatalog = productCatalog;

    }


    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GeneralViewHolder(View.inflate(parent.getContext(), R.layout.inventory_list, null), GeneralViewHolder.Adapter.category, listener);
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        Category category = list.get(position);
        holder.name.setText(category.getName());
        holder.price.setText(productCatalog.getProductsQuantityInCategory(category.getId()));
    }


    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
