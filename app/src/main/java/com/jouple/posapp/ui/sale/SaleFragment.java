package com.jouple.posapp.ui.sale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.component.BaseRecyclerAdapter;
import com.jouple.posapp.ui.component.SalesAdapter;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.inventory.MainUI;
import com.jouple.posapp.ui.utils.AppButton;
import com.jouple.posapp.ui.utils.AppTextView;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.ArrayList;
import java.util.List;

import static com.jouple.posapp.ui.component.PagerAdapter.mainUI;
import static com.jouple.posapp.ui.component.PagerAdapter.saleFragment;

/**
 * UI for Sale operation.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class SaleFragment extends UpdatableFragment {


    private Register register;
    //    private ArrayList<Map<String, String>> saleList;
    // private ListView saleListView;
    private RecyclerView saleRV;
    private AppButton addMisc;
    //private Button clearButton;
    private AppTextView totalPrice, addLabel, vat, discount;
    private Button endButton;
    private ImageView cartImage;
    private View saleHeader, line;
    private Resources res;
    private List<LineItem> list = new ArrayList<>();
    private Product misc;

    public SaleFragment() {
    }

    public static SaleFragment getInstance() {

        return (SaleFragment) saleFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        try {
            register = Register.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.layout_sale, container, false);

        res = getResources();
        //  saleListView = view.findViewById(R.id.sale_List);
        saleRV = view.findViewById(R.id.saleRV);
        cartImage = view.findViewById(R.id.cart);

        totalPrice = view.findViewById(R.id.totalPrice);
        vat = view.findViewById(R.id.taxBox);
        //     clearButton = view.findViewById(R.id.clearButton);
        endButton = view.findViewById(R.id.endButton);
        addLabel = view.findViewById(R.id.addLabel);
        line = view.findViewById(R.id.footer);
        addMisc = view.findViewById(R.id.addmisc);


//        AppTextView name = view.findViewById(R.id.name);
//        AppTextView quantity = view.findViewById(R.id.quantity);
//        AppTextView tax = view.findViewById(R.id.tax);
        discount = view.findViewById(R.id.discountBox);
//        AppTextView price = view.findViewById(R.id.price);
//        name.setBoldTextEnabled(true);
//        quantity.setBoldTextEnabled(true);
//        tax.setBoldTextEnabled(true);
//        discount.setBoldTextEnabled(true);
//        price.setBoldTextEnabled(true);
//        View line = view.findViewById(R.id.line);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) line.getLayoutParams();
//        params.setMargins(0, 0, 0, 0);
//        line.setLayoutParams(params);

        addMisc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.size() > 0)
                    addMisc(view);
            }
        });
        initUI();
        return view;
    }

    private void addMisc(View view) {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_misc);
        SleekUI.setHeader("Add Misc", dialog, true);
        final EditText name = dialog.findViewById(R.id.name);
        final EditText quantity = dialog.findViewById(R.id.quantity);
        final EditText price = dialog.findViewById(R.id.price);
        dialog.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "Success";
                String nameS = name.getText().toString();
                String quantityS = quantity.getText().toString();
                String priceS = price.getText().toString();
                if (price.length() > 0 && quantityS.length() > 0) {
                    misc = new Product("Misc", "no image bro", "", list.get(0).getProduct().getCategoryID(), "", Double.parseDouble(priceS), 0, 0);

                    try {
                        misc.setId(Inventory.getInstance().getProductCatalog().addProduct(misc.getName(), misc.getImage(), misc.getDescription(), misc.getCategoryID(), misc.getBarcode(), misc.getUnitPrice()));
                    } catch (NoDaoSetException e) {

                        e.printStackTrace();
                    }

                    register.addItem(misc, Integer.parseInt(quantityS));

                    dialog.dismiss();
                    MainUI.refresh((MainUI) mainUI);


                } else {
                    message = "All Fields Required";
                }
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();

            }
        });

        dialog.findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name.setText("");
                quantity.setText("");
                price.setText("");
            }
        });
        dialog.show();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        SleekUI.initRecyclerView(saleRV, true);

        // AppTextView id = view.findViewById(R.id.name);
        // AppTextView date = view.findViewById(R.id.quantity);
        AppTextView tax = view.findViewById(R.id.tax);
        // AppTextView dis = view.findViewById(R.id.discount);
        // AppTextView price = view.findViewById(R.id.price);
        saleHeader = view.findViewById(R.id.saleListView);
        AppTextView category = view.findViewById(R.id.categoryName);
        tax.setVisibility(View.GONE);
        category.setVisibility(View.GONE);
        view.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainActivity().addMoreItems(view);
            }
        });
        //  view.findViewById(R.id.image).setVisibility(View.GONE);
    }

    /**
     * Initiate this UI.
     */
    private void initUI() {

//        saleListView.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                showEditPopup(arg1, arg2);
//            }
//        });


        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (register.hasSale()) {

                    choosePaymentType(v);

                } else {

                    Toast.makeText(getActivity(), res.getString(R.string.hint_empty_sale), Toast.LENGTH_SHORT).show();
                }
            }
        });

//        clearButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!register.hasSale() || register.getCurrentSale().getAllLineItem().isEmpty()) {
//                    Toast.makeText(getActivity(), res.getString(R.string.hint_empty_sale), Toast.LENGTH_SHORT).show();
//                } else {
//                    showConfirmClearDialog();
//                }
//            }
//        });
    }

    /**
     * Show list
     */
    private void showList() {


//        for (LineItem line : list) {
//            saleList.add(line.toMap());
//        }

        SalesAdapter adapter = new SalesAdapter(list);
        adapter.setPrintingUI(false);
        saleRV.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnListClickListener() {
            @Override
            public void OnClick(View v, int position) {
                showEditPopup(list.get(position));
            }

            @Override
            public boolean OnLongClick(View v, int position) {
                return false;
            }
        });


        //      ProductsFragment fragment = (ProductsFragment) productsFragment;
//        fragment.setCounter(this.list.size() + "");
//        SimpleAdapter sAdap;
//        sAdap = new SimpleAdapter(getActivity(), saleList,
//                R.layout.listview_lineitem, new String[]{"name", "quantity", "price"}, new int[]{R.id.name, R.id.quantity, R.id.price});
//        saleListView.setAdapter(sAdap);
    }

    /**
     * Try parsing String to double.
     *
     * @param value
     * @return true if can parse to double.
     */
    public boolean tryParseDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Show edit popup.
     * <p>
     * //  * @param anchorView
     *
     * @param lineItem
     */
    public void showEditPopup(LineItem lineItem) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("lineItem", lineItem);
        bundle.putInt("sale_id", register.getCurrentSale().getId());
        EditFragmentDialog newFragment = new EditFragmentDialog();
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), "");


    }

    private void choosePaymentType(View view) {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.payment_type_dialog);
        SleekUI.setHeader("Select Payment Method", dialog, false);
        dialog.show();
        dialog.findViewById(R.id.atm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.paymentType = "Credit Card";
                showPaymentDialog(view);
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.cash).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.paymentType = "Cash";
                showPaymentDialog(view);
                dialog.dismiss();
            }
        });

    }

    /**
     * Show popup
     *
     * @param view
     */
    private void showPaymentDialog(View view) {
        MainUI.loaderDialog(view.getContext()).show();
        Bundle bundle = new Bundle();
        bundle.putDouble("tp", register.getTotal());
        bundle.putDouble("tax", register.getCurrentSale().getTotalTax());
        bundle.putDouble("discount", register.getCurrentSale().getTotalDiscount());
        bundle.putInt("order", register.getCurrentSale().getId());
        PaymentFragmentDialog newFragment = new PaymentFragmentDialog(list);
        newFragment.setMisc(misc);
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), "");
    }


//    private Register register() {
//        if (register == null) {
//            try {
//                return Register.getInstance();
//            } catch (NoDaoSetException e) {
//                e.printStackTrace();
//            }
//        }
//        return this.register;
//    }

    @Override
    public void update() {
        list = register.getCurrentSale().getAllLineItem();
        if (list.size() > 0) {
            addMisc.setVisibility(View.VISIBLE);
            saleHeader.setVisibility(View.VISIBLE);
            endButton.setVisibility(View.VISIBLE);
            cartImage.setVisibility(View.GONE);
            addLabel.setVisibility(View.INVISIBLE);
            totalPrice.setVisibility(View.VISIBLE);
            vat.setVisibility(View.VISIBLE);
            discount.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
        } else {
            addMisc.setVisibility(View.GONE);
            saleHeader.setVisibility(View.INVISIBLE);
            totalPrice.setVisibility(View.GONE);
            vat.setVisibility(View.GONE);
            discount.setVisibility(View.GONE);
            cartImage.setVisibility(View.VISIBLE);
            addLabel.setVisibility(View.VISIBLE);
            endButton.setVisibility(View.GONE);
            line.setVisibility(View.INVISIBLE);

        }
        if (register.hasSale()) {

            vat.setText("VAT " + Constant.decimalPlaces(register.getCurrentSale().getTotalTax()) + "");
            discount.setText("Discount - " + Constant.decimalPlaces(register.getCurrentSale().getTotalDiscount()) + "");
            totalPrice.setText("Total " + Constant.decimalPlaces(register.getTotal()) + "");
            SleekUI.getLabelView(getView(), R.id.subtotal).setText("Subtotal " + String.valueOf(register.getTotal() - register.getCurrentSale().getTotalTax() + register.getCurrentSale().getTotalDiscount()) + "");
        }
        showList();
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    /**
     * Show confirm or clear dialog.
     */
    private void showConfirmClearDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(res.getString(R.string.dialog_clear_sale));
        dialog.setPositiveButton(res.getString(R.string.no), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialog.setNegativeButton(res.getString(R.string.clear), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                register.cancleSale();
                update();
            }
        });

        dialog.show();
    }

}
