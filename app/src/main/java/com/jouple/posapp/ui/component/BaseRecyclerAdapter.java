package com.jouple.posapp.ui.component;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by macbook on 09/11/2017.
 */

public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<GeneralViewHolder> {
    OnListClickListener listener;

    public void setOnItemClickListener(OnListClickListener listener) {
        this.listener = listener;
    }


    public interface OnListClickListener {
        void OnClick(View v, int position);

        boolean OnLongClick(View v, int position);
    }

}
