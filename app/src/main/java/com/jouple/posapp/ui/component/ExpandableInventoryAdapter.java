package com.jouple.posapp.ui.component;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Category;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.ui.utils.AppTextView;
import com.jouple.posapp.ui.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by macbook on 04/12/2017.
 */

public class ExpandableInventoryAdapter extends ExpandableRecyclerAdapter<Category, Product, ExpandableInventoryAdapter.CVH, ExpandableInventoryAdapter.PVH> {

    private OnEnhancedCategoryAdapterClickListener onEnhancedCategoryAdapterClickListener;
    private RecyclerView recyclerView;

    /**
     * Primary constructor. Sets up {@link #mParentList} and {@link #mFlatItemList}.
     * <p>
     * Any changes to {@link #mParentList} should be made on the original instance, and notified via
     * {@link #notifyParentInserted(int)}
     * {@link #notifyParentRemoved(int)}
     * {@link #notifyParentChanged(int)}
     * {@link #notifyParentRangeInserted(int, int)}
     * {@link #notifyChildInserted(int, int)}
     * {@link #notifyChildRemoved(int, int)}
     * {@link #notifyChildChanged(int, int)}
     * methods and not the notify methods of RecyclerView.Adapter.
     *
     * @param recyclerView
     * @param parentList   List of all parents to be displayed in the RecyclerView that this
     */
    public ExpandableInventoryAdapter(RecyclerView recyclerView, @NonNull List<Category> parentList) {
        super(parentList);
        this.recyclerView = recyclerView;
    }

    public void setOnEnhancedCategoryAdapterClickListener(OnEnhancedCategoryAdapterClickListener onEnhancedCategoryAdapterClickListener) {
        this.onEnhancedCategoryAdapterClickListener = onEnhancedCategoryAdapterClickListener;
    }

    @NonNull
    @Override
    public CVH onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        return new CVH(LayoutInflater.from(parentViewGroup.getContext()).inflate(R.layout.adapter_expandable_category, parentViewGroup, false));
    }

    @NonNull
    @Override
    public PVH onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        return new PVH(LayoutInflater.from(childViewGroup.getContext()).inflate(R.layout.adapter_expandable_products, childViewGroup, false));
    }

    @Override
    public void onBindParentViewHolder(@NonNull CVH parentViewHolder, int parentPosition, @NonNull Category parent) {

        parentViewHolder.categoryName.setText(parent.getName());

        parentViewHolder.productCount.setText(String.valueOf(parent.getChildList().size()));

    }

    @Override
    public void onBindChildViewHolder(@NonNull PVH holder, int parentPosition, int childPosition, @NonNull Product product) {

        Picasso.with(holder.itemView.getContext()).load(product.getImage()).placeholder(R.color.btn_color).into(holder.iv);
        holder.productName.setText(product.getName());
        holder.price.setText(Constant.decimalPlaces(product.getUnitPrice()));
    }

    public interface OnEnhancedCategoryAdapterClickListener {
        void OnProductClick(Product product);

        boolean OnProductLongClick(Product product);

        boolean OnCategoryLongClick(View view, int position);
    }

    class CVH extends ParentViewHolder {
        AppTextView categoryName, productCount;
        int lastExpandedPosition = -1;
        ImageView arrow;
        //  RelativeLayout rootLayout;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        CVH(@NonNull final View itemView) {
            super(itemView);
            //     rootLayout = itemView.findViewById(R.id.root);
            categoryName = itemView.findViewById(R.id.name);
            productCount = itemView.findViewById(R.id.counter);
            arrow = itemView.findViewById(R.id.arrow);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getParentAdapterPosition();
                    int size = getParentList().size();
                    if (position == size - 1) {
                        recyclerView.scrollToPosition(getItemCount());
                    }

                    if (isExpanded())
                        collapseView();
                    else expandView();


                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener()

            {
                @Override
                public boolean onLongClick(View view) {
                    boolean b = false;
                    if (onEnhancedCategoryAdapterClickListener != null)
                        b = onEnhancedCategoryAdapterClickListener.OnCategoryLongClick(view, getParentAdapterPosition());
                    return b;
                }
            });

        }

        @Override
        public boolean shouldItemViewClickToggleExpansion() {
            return false;
        }
    }

    class PVH extends ChildViewHolder {
        AppTextView productName, price, category;
        ImageView iv, action;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        PVH(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            category = itemView.findViewById(R.id.category);
            action = itemView.findViewById(R.id.action);
            iv = itemView.findViewById(R.id.image);
            action.setVisibility(View.VISIBLE);
            category.setVisibility(View.GONE);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return onEnhancedCategoryAdapterClickListener != null && onEnhancedCategoryAdapterClickListener.OnProductLongClick((Product) getChild());

                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onEnhancedCategoryAdapterClickListener != null)
                        onEnhancedCategoryAdapterClickListener.OnProductClick((Product) getChild());
                }
            });
        }
    }

}
