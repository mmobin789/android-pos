package com.jouple.posapp.ui.inventory;


import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Category;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.activities.ProductDetailActivity;
import com.jouple.posapp.ui.component.ExpandableInventoryAdapter;
import com.jouple.posapp.ui.component.OnFragmentViewListener;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.utils.AppButton;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.ArrayList;
import java.util.List;

import static com.jouple.posapp.ui.component.PagerAdapter.categoryFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.productsFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.saleFragment;
import static com.jouple.posapp.ui.utils.Constant.isAdmin;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryFragment#} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends UpdatableFragment {
    public EditText searchBox;
    //  private CategoryAdapter adapter;
    ExpandableInventoryAdapter adapter;
    OnFragmentViewListener listener;
    /**
     * Show list.
     *
     * @param list
     */
    int lastExpandedPosition;
    // TODO: Rename parameter arguments, choose names that match
    private Button addCategoryButton, showProducts;
    private RecyclerView listView;
    private List<Category> categoryList = new ArrayList<>();
    // private AppTextView label3, label1, label2;
    private Button scanButton;
    private AppButton search;
    private MainUI mainUI;
    private Resources res;
    private Register register;


    public CategoryFragment() {
    }

    public static CategoryFragment getInstance() {
        if (categoryFragment == null)
            categoryFragment = new CategoryFragment();

        return (CategoryFragment) categoryFragment;
    }

    public void setMainUI(MainUI mainUI) {
        this.mainUI = mainUI;
    }

    public ProductCatalog getProductCatalog() {
        try {
            return Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(this.getClass().getName(), "init");
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.layout_inventory, container, false);
        res = getResources();
        listView = view.findViewById(R.id.productRV);
        search = view.findViewById(R.id.filter);
        search.setTypeface(SleekUI.fontAwesome(getContext()));
        SleekUI.initRecyclerView(listView, true);
        addCategoryButton = view.findViewById(R.id.addProductButton);
        showProducts = view.findViewById(R.id.showProducts);
        scanButton = view.findViewById(R.id.scanButton);
        searchBox = view.findViewById(R.id.searchBox);
        view.findViewById(R.id.navigation_header_container).setVisibility(View.GONE);
        view.findViewById(R.id.line).setVisibility(View.GONE);


        TextView label1 = view.findViewById(R.id.name);
        label1.setText(getString(R.string.name));
//        label1.setGravity(Gravity.START);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) label1.getLayoutParams();
//        params.setMarginStart(res.getDimensionPixelOffset(R.dimen._16sdp));
//        label1.setLayoutParams(params);
        TextView label2 = view.findViewById(R.id.price);
        TextView label3 = view.findViewById(R.id.category);
        TextView label4 = view.findViewById(R.id.action);
        //  label2.setVisibility(View.GONE);
        // label3.setVisibility(View.GONE);
        label2.setText("Products");
        label3.setText("Action");
        label4.setVisibility(View.GONE);
        view.findViewById(R.id.dropdown).setVisibility(View.GONE);

        initUI(view);

        return view;
    }

    private void moveToProductsPage() {
        assert getParentFragment() != null;
        ((MainUI) getParentFragment()).inventory.setCurrentItem(1);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        try {
            register = Register.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToProductsPage();
            }
        });

    }

    private boolean alreadyExists(String categoryName) {
        boolean b = false;
        for (Category category : categoryList) {
            b = categoryName.equals(category.getName());
        }
        return b;
    }

    public void addCategoryDialog(final View v) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_addcategory);
        SleekUI.setHeader("Add Category", dialog, true);
        final EditText categoryBox = dialog.findViewById(R.id.categoryBox);
        final EditText taxBox = dialog.findViewById(R.id.taxBox);
        final EditText discountBox = dialog.findViewById(R.id.discountBox);
        dialog.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String category = categoryBox.getText().toString();
                boolean exists = alreadyExists(category);
                if (category.equals("")) {

                    Toast.makeText(v.getContext(), "Name required",
                            Toast.LENGTH_SHORT).show();
                }
                if (exists) {
                    Toast.makeText(v.getContext(), "Already Exists",
                            Toast.LENGTH_SHORT).show();
                }
                if (!category.equals("") && !exists) {
                    String taxS = taxBox.getText().toString().trim();
                    String discountS = discountBox.getText().toString().trim();
                    double tax = 0.0, discount = 0.0;
                    if (!taxS.equals(""))
                        tax = Double.parseDouble(taxS);
                    if (!discountS.equals(""))
                        discount = Double.parseDouble(discountS);

                    boolean success = getProductCatalog().addCategory(categoryBox.getText().toString(), tax, discount);
                    if (success) {
                        Toast.makeText(v.getContext(),
                                v.getContext().getString(R.string.success) + ", "
                                        + categoryBox.getText().toString(),
                                Toast.LENGTH_SHORT).show();
                        if (((ProductsFragment) productsFragment).addProductDialogFragment != null)
                            ((ProductsFragment) productsFragment).addProductDialogFragment.checkForCategories();
                        update();
                        dialog.dismiss();

                    } else {
                        Toast.makeText(v.getContext(),
                                v.getContext().getString(R.string.fail),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        dialog.findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryBox.setText("");
                discountBox.setText("");
                taxBox.setText("");
            }
        });

        dialog.show();
    }

    /**
     * Initiate this UI.
     *
     * @param view
     */
    private void initUI(View view) {
        view.findViewById(R.id.cart).setVisibility(View.GONE);
        // showProducts.setBackground(null);
        search.setEnabled(false);
//        LinearLayout.LayoutParams params = (RelativeLayout.LayoutParams) showProducts.getLayoutParams();
//         params.removeRule(RelativeLayout.CENTER_VERTICAL);
//        params.removeRule(RelativeLayout.ALIGN_PARENT_END);
//        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
//        showProducts.setLayoutParams(params);

//        label1.setText(res.getString(R.string.category));
//        label2.setVisibility(View.GONE);
//        label3.setVisibility(View.GONE);
        showProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainActivity().getViewPager().setCurrentItem(1, true);
            }
        });
        addCategoryButton.setText("Add New Category");
        addCategoryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addCategoryDialog(v);
            }
        });

        showList(getProductCatalog().getAllCategories());
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    MainUI.searchTxt = editable.toString();
                    search.setEnabled(true);

                } else {
                    search.setEnabled(false);
                }
            }
        });


        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(getMainActivity());
                scanIntegrator.initiateScan();
            }
        });
        checkAdmin();

    }

    private void checkAdmin() {
        if (isAdmin)

        {
            scanButton.setEnabled(true);
        } else {
            scanButton.setEnabled(false);

        }

    }

    private void editCategoryDialog(View v, final int position) {
        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_addcategory);
        final EditText categoryBox = dialog.findViewById(R.id.categoryBox);
        final EditText discountBox = dialog.findViewById(R.id.discountBox);
        final EditText taxBox = dialog.findViewById(R.id.taxBox);
        Category category = categoryList.get(position);
        categoryBox.setText(category.getName());
        discountBox.setText("" + category.getDiscount() + "");
        taxBox.setText("" + category.getTax() + "");
        dialog.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = categoryBox.getText().toString().trim();
                if (name.length() > 0) {
                    Category category = categoryList.get(position);
                    category.setName(name);
                    String taxS = taxBox.getText().toString();
                    String discountS = discountBox.getText().toString();
                    double tax = 0, discount = 0;
                    if (!discountS.equals(""))
                        discount = Double.parseDouble(discountS);
                    if (!taxS.equals(""))
                        tax = Double.parseDouble(taxS);
                    category.setTax(tax);
                    category.setDiscount(discount);
                    getProductCatalog().editCategory(category);
                    dialog.dismiss();
                    adapter.notifyItemChanged(position);
                } else {
                    Toast.makeText(view.getContext(), "name required", Toast.LENGTH_SHORT).show();
                }

            }
        });
        dialog.findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryBox.setText("");
                taxBox.setText("");
                discountBox.setText("");
            }
        });
        dialog.show();
    }

    private boolean categoryMenu(final View v, final int position) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.category_menu, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_edit:
                        editCategoryDialog(v, position);
                        break;

                    case R.id.removeButton:
                        getProductCatalog().removeCategory(categoryList.get(position));
                        categoryList.remove(position);
                        adapter.notifyParentRemoved(position);
                        break;
                }

                return true;
            }
        });

        return true;
    }

    public void setFragmentViewListener(OnFragmentViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (listener != null)
            listener.OnFragmentViewNonNull(getView());

    }

    private void showList(List<Category> list) {
        adapter = null;
        categoryList.clear();
        categoryList.addAll(list);
        lastExpandedPosition = -1;
        adapter = new ExpandableInventoryAdapter(listView, categoryList);
        adapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onParentExpanded(int parentPosition) {
                if (lastExpandedPosition != -1)
                    adapter.collapseParent(lastExpandedPosition);
                lastExpandedPosition = parentPosition;
            }


            @Override
            public void onParentCollapsed(int parentPosition) {
                if (categoryList.size() == 1)
                    lastExpandedPosition = -1;
            }
        });

        adapter.setOnEnhancedCategoryAdapterClickListener(new ExpandableInventoryAdapter.OnEnhancedCategoryAdapterClickListener() {
            @Override
            public void OnProductClick(Product product) {
                Log.i("OnProductClick", product.toString());
                register.addItem(Inventory.getChargedProduct(product, getContext()), 1);
                //   register.addItem(product, 1);
                saleFragment.update();
            }

            @Override
            public boolean OnProductLongClick(Product product) {
                Intent productDetail = new Intent(getContext(), ProductDetailActivity.class);
                productDetail.putExtra("id", product.getId());
                startActivity(productDetail);
                return true;
            }

            @Override
            public boolean OnCategoryLongClick(View view, int position) {
                boolean b = false;
                if (isAdmin)
                    b = categoryMenu(view, position);
                return b;

            }
        });
        if (listView == null)
            listView = mainUI.catView.findViewById(R.id.productRV);

        listView.setAdapter(adapter);


//            adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnListClickListener() {
//                @Override
//                public void OnClick(View v, int position) {
//
//                    viewPager.setCurrentItem(1, true);
//                    fragment.searchProductsByCategoryID(categoryList.get(position).getId());
//
//                }
//
//                @Override
//                public boolean OnLongClick(View v, int position) {
//                    boolean b = false;
//                    if (isAdmin)
//                        b = categoryMenu(v, position);
//                    return b;
//                }
//            });


    }


    /**
     * Search.
     */
    private void search() {
        String search = searchBox.getText().toString();
        switch (search) {
//            case "/clear":
//                DatabaseExecutor.getInstance().dropAllData();
//                searchBox.setText("");
//                break;
            case "":
                showList(getProductCatalog().getAllCategories());
                break;
            default:
                List<Category> result = getProductCatalog().searchCategory(search);
                showList(result);
                if (result.isEmpty()) {

                }
                break;
        }

    }

    public void getBarcodeResult(IntentResult scanningResult) {


        String scanContent = scanningResult.getContents();
        searchBox.setText(scanContent);

    }


    @Override
    public void update() {
        //search();
        showList(getProductCatalog().getAllCategories());
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

}
