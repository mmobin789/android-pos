package com.jouple.posapp.ui.sale;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.DateTimeStrategy;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.sale.Admin;
import com.jouple.posapp.domain.sale.QuickLoadSale;
import com.jouple.posapp.domain.sale.Sale;
import com.jouple.posapp.domain.sale.SaleLedger;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.activities.SaleDetailActivity;
import com.jouple.posapp.ui.component.BaseRecyclerAdapter;
import com.jouple.posapp.ui.component.ReportsAdapter;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.utils.AppTextView;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * UI for showing sale's record.
 *
 * @author Refresh Team
 */
public class ReportFragment extends UpdatableFragment {

    public static final int DAILY = 0;
    public static final int WEEKLY = 1;
    public static final int MONTHLY = 2;
    public static final int YEARLY = 3;
    //  List<Map<String, String>> saleList;
    private List<Sale> saleList;
    private SaleLedger saleLedger;
    private RecyclerView saleLedgerListView;
    private AppTextView totalBox, period;
    private Spinner spinner;
    private Button previousButton;
    private Button nextButton;
    private AppTextView currentBox;
    private Calendar currentTime;
    private DatePickerDialog datePicker;
    private int menuSelected;

    public ReportFragment() {
    }

    private void showPeriods(View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v);
        String[] data = getMainActivity().getResources().getStringArray(R.array.period);
        Menu menu = popup.getMenu();
        for (int i = 0; i < data.length; i++) {
            menu.add(Menu.NONE, i, Menu.NONE, data[i]);

        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                menuSelected = menuItem.getItemId();
                period.setText(menuItem.getTitle());
                update();
                return true;
            }
        });

//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        try {
            saleLedger = SaleLedger.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.layout_report, container, false);

        previousButton = view.findViewById(R.id.previousButton);
        nextButton = view.findViewById(R.id.nextButton);
        currentBox = view.findViewById(R.id.currentBox);
        saleLedgerListView = view.findViewById(R.id.saleListView);
        totalBox = view.findViewById(R.id.totalBox);
        period = view.findViewById(R.id.period);
        spinner = view.findViewById(R.id.spinner1);
        view.findViewById(R.id.image).setVisibility(View.GONE);
        SleekUI.initRecyclerView(saleLedgerListView, true);
        initUI();
        view.findViewById(R.id.add).setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        AppTextView id = view.findViewById(R.id.name);
        AppTextView date = view.findViewById(R.id.quantity);
        AppTextView tax = view.findViewById(R.id.tax);
        AppTextView dis = view.findViewById(R.id.discount);
        AppTextView price = view.findViewById(R.id.price);
        AppTextView cat = view.findViewById(R.id.categoryName);
        dis.setVisibility(View.GONE);
        cat.setVisibility(View.GONE);
        id.setText(R.string.invoice);
        date.setText(R.string.date);
        price.setText(R.string.total);
        tax.setText("vat".toUpperCase());
//        View line = view.findViewById(R.id.line);
//        line.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.gray));
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) line.getLayoutParams();
//        params.setMargins(0, 0, 0, 0);
//        line.setLayoutParams(params);


    }


    private void createReport() {
        final List<Sale> saleList = saleLedger.getAllSale();
        if (saleList.size() > 0) {
            final Admin admin = Constant.getAdmin(getContext());
            final HandlerThread handlerThread = new HandlerThread("Report Creator Thread");
            handlerThread.start();
            Handler handler = new Handler(handlerThread.getLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    createExcelReport(admin, saleList);

                    handlerThread.quit();
                }
            });


        }
    }

    private void createExcelReport(Admin admin, List<Sale> saleList) {
        // List<LineItem> soldItems = sale.getAllLineItem();
        XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(DateTimeStrategy.getCurrentTime().substring(0, 10));
        Row header = sheet.createRow(0);
        Cell cell = header.createCell(0);
        cell.setCellValue("Vendor");
        cell = header.createCell(1);
        cell.setCellValue(admin.getVendor());
        cell = header.createCell(2);
        cell.setCellValue("User");
        cell = header.createCell(3);
        cell.setCellValue(admin.getName());
        Row header2 = sheet.createRow(1);
        cell = header2.createCell(0);
        cell.setCellValue("TAB");
        cell = header2.createCell(1);
        cell.setCellValue(admin.getTab());
        for (int c = 0; c < 4; c++)
            sheet.setColumnWidth(c, (15 * 500));

        int lastSaleItems = 2;
        for (int i = 0; i < saleList.size(); i++) {
            Sale sale = saleList.get(i);
            Row row1 = sheet.createRow(lastSaleItems);  // column heading
            cell = row1.createCell(0);

            cell.setCellValue("Invoice#");
            cell = row1.createCell(1);
            cell.setCellValue("Date");
            //  cell2.setCellStyle(cs);
            cell = row1.createCell(2);
            //     cell3.setCellStyle(cs);
            cell.setCellValue("Total");
            Row row2 = sheet.createRow(row1.getRowNum() + 1);
            cell = row2.createCell(0);
            cell.setCellValue(sale.getId() + "");
            cell = row2.createCell(1);
            cell.setCellValue(sale.getEndTime());
            cell = row2.createCell(2);
            QuickLoadSale quickLoadSale = (QuickLoadSale) sale;
            cell.setCellValue(quickLoadSale.getTotal() + "");
            Row row3 = sheet.createRow(row2.getRowNum() + 1);
            cell = row3.createCell(0);
            cell.setCellValue("Items");
            cell = row3.createCell(1);
            cell.setCellValue("Quantity");
            cell = row3.createCell(2);
            cell.setCellValue("Price");
            cell = row3.createCell(3);
            cell.setCellValue("VAT");
            List<LineItem> lineItems = saleLedger.getSaleById(sale.getId()).getAllLineItem();
            Log.i("lineItems", lineItems.size() + "");
            for (int k = 0; k < lineItems.size(); k++) {
                LineItem lineItem = lineItems.get(k);
                Row row4 = sheet.createRow(row3.getRowNum() + 1 + k);
                Product product = lineItem.getProduct();
                cell = row4.createCell(0);
                cell.setCellValue(product.getName());
                cell = row4.createCell(1);
                cell.setCellValue(lineItem.getQuantity() + "");
                cell = row4.createCell(2);
                cell.setCellValue(String.valueOf(lineItem.getPriceAtSale() * lineItem.getQuantity()));
                cell = row4.createCell(3);
                cell.setCellValue(String.valueOf(product.getTax() * lineItem.getQuantity()));
                lastSaleItems = row4.getRowNum() + 1;

            }
        }
        // Create a path where we will place our List of objects on external storage


        File reportsFolder = new File(Environment.getExternalStorageDirectory(), "MATB reports");
        if (!reportsFolder.exists())

        {
            reportsFolder.mkdir();
            Log.i("report", "report Directory created" + reportsFolder.getPath());

        }

        File file = new File(reportsFolder, " MATB Report#" + DateTimeStrategy.getCurrentTime().replaceAll("[^a-zA-Z0-9]+", "") + ".xlsx");
        FileOutputStream os;

        try

        {

            os = new FileOutputStream(file);
            workbook.write(os);
            Log.w("FileUtils", "Wrote file" + file.getPath());
            os.close();
            //  ReportSync.Companion.setReport(file);

        } catch (
                IOException e)

        {
            Log.e("FileUtils", "Error writing " + file, e);

        } catch (
                Exception e)

        {
            Log.e("FileUtils", "Failed to save file", e);
        }


    }

    /**
     * Initiate this UI.
     */

    private void initUI() {
        period.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showPeriods(view);
            }
        });
        currentTime = Calendar.getInstance();
        datePicker = new DatePickerDialog(getMainActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                currentTime.set(Calendar.YEAR, y);
                currentTime.set(Calendar.MONTH, m);
                currentTime.set(Calendar.DAY_OF_MONTH, d);
                update();
            }
        }, currentTime.get(Calendar.YEAR), currentTime.get(Calendar.MONTH), currentTime.get(Calendar.DAY_OF_MONTH));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.period, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

        currentBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.show();
            }
        });


        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addDate(-1);
            }
        });

        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addDate(1);
            }
        });

//        saleLedgerListView.setOnItemClickListener(new OnItemClickListener() {
//            public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
//                String id = saleList.get(position).get("id");
//                Intent newActivity = new Intent(getActivity().getBaseContext(), SaleDetailActivity.class);
//                newActivity.putExtra("id", id);
//                startActivity(newActivity);
//            }
//        });

    }


    /**
     * Show list.
     *
     * @param list
     */

    private void showList(List<Sale> list) {

        saleList = new ArrayList<>();
        saleList.clear();
        saleList.addAll(list);
//        for (Sale sale : list) {
//            saleList.add(sale.toMap());
//        }
//
//        SimpleAdapter sAdap = new SimpleAdapter(getActivity(), saleList,
//                R.layout.listview_report, new String[]{"id", "startTime", "total"},
//                new int[]{R.id.sid, R.id.startTime, R.id.total});


        ReportsAdapter adapter = new ReportsAdapter(saleList);
        saleLedgerListView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnListClickListener() {
            @Override
            public void OnClick(View v, int position) {
                Intent reportDetail = new Intent(v.getContext(), SaleDetailActivity.class);
                reportDetail.putExtra("id", saleList.get(position).getId());
                startActivity(reportDetail);
            }

            @Override
            public boolean OnLongClick(View v, int position) {
//                boolean b = exportToExcel(position);
//                if (b) {
//                    Toast.makeText(v.getContext(), "success", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(v.getContext(), "failed", Toast.LENGTH_SHORT).show();
//                }
                return false;
            }
        });

    }


//    private void startSendingReportToGoogleDrive() {
//
//        getMainActivity().startService(new Intent(getMainActivity(), ReportSync.class));
//
//
//    }

    @Override
    public void update() {
        int period = menuSelected;
        //spinner.getSelectedItemPosition();
        List<Sale> list;
        Calendar cTime = (Calendar) currentTime.clone();
        Calendar eTime = (Calendar) currentTime.clone();

        if (period == DAILY) {
            currentBox.setText(" [" + DateTimeStrategy.getSQLDateFormat(currentTime) + "] ");
            currentBox.setTextSize(16);
        } else if (period == WEEKLY) {
            while (cTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                cTime.add(Calendar.DATE, -1);
            }

            String toShow = " [" + DateTimeStrategy.getSQLDateFormat(cTime) + "] ~ [";
            eTime = (Calendar) cTime.clone();
            eTime.add(Calendar.DATE, 7);
            toShow += DateTimeStrategy.getSQLDateFormat(eTime) + "] ";
            currentBox.setTextSize(16);
            currentBox.setText(toShow);
        } else if (period == MONTHLY) {
            cTime.set(Calendar.DATE, 1);
            eTime = (Calendar) cTime.clone();
            eTime.add(Calendar.MONTH, 1);
            eTime.add(Calendar.DATE, -1);
            currentBox.setTextSize(18);
            currentBox.setText(" [" + currentTime.get(Calendar.YEAR) + "-" + (currentTime.get(Calendar.MONTH) + 1) + "] ");
        } else if (period == YEARLY) {
            cTime.set(Calendar.DATE, 1);
            cTime.set(Calendar.MONTH, 0);
            eTime = (Calendar) cTime.clone();
            eTime.add(Calendar.YEAR, 1);
            eTime.add(Calendar.DATE, -1);
            currentBox.setTextSize(20);
            currentBox.setText(" [" + currentTime.get(Calendar.YEAR) + "] ");
        }
        currentTime = cTime;
        list = saleLedger.getAllSaleDuring(cTime, eTime);
        double total = 0;
        for (Sale sale : list)
            total += sale.getTotal();

        totalBox.setText(Constant.decimalPlaces(total));
        showList(list);

        //createReport();
    }

    @Override
    public void onResume() {
        super.onResume();
        // update();
        // it shouldn't call update() anymore. Because super.onResume()
        // already fired the action of spinner.onItemSelected()
    }

    /**
     * Add date.
     *
     * @param increment
     */
    private void addDate(int increment) {
        int period = menuSelected;
        if (period == DAILY) {
            currentTime.add(Calendar.DATE, increment);
        } else if (period == WEEKLY) {
            currentTime.add(Calendar.DATE, 7 * increment);
        } else if (period == MONTHLY) {
            currentTime.add(Calendar.MONTH, increment);
        } else if (period == YEARLY) {
            currentTime.add(Calendar.YEAR, increment);
        }
        update();
    }

}
