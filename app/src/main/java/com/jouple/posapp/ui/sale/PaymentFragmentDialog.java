package com.jouple.posapp.ui.sale;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.ui.inventory.MainUI;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.List;

/**
 * A dialog for input a money for sale.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class PaymentFragmentDialog extends DialogFragment {

    private EditText input;
    private List<LineItem> list;
    private Product misc;


    /**
     * Construct a new PaymentFragmentDialog.
     *
     * @param list
     */
    public PaymentFragmentDialog(List<LineItem> list) {
        this.list = list;
        setRetainInstance(true);

    }

    public PaymentFragmentDialog() {
    }

    public void setMisc(Product misc) {
        this.misc = misc;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.dialog_payment, container, false);
        SleekUI.setHeader("Checkout", v);
        assert getArguments() != null;
        final double tp = getArguments().getDouble("tp");
        final double tax = getArguments().getDouble("tax");
        final double discount = getArguments().getDouble("discount");
        final double subTotal = tp - tax + discount;
        input = v.findViewById(R.id.dialog_saleInput);
        TextView type = v.findViewById(R.id.type);
        type.setText(Constant.paymentType);
        TextView subTotalTV = v.findViewById(R.id.subtotal);
        TextView totalPrice = v.findViewById(R.id.payment_total);
        TextView taxTV = v.findViewById(R.id.tax);
        TextView discountTV = v.findViewById(R.id.discount);
        totalPrice.setText(Constant.decimalPlaces(tp));
        taxTV.setText(Constant.decimalPlaces(tax));
        discountTV.setText(Constant.decimalPlaces(discount));
        subTotalTV.setText(Constant.decimalPlaces(subTotal));
        Button confirmButton = v.findViewById(R.id.confirmButton);
        Button cancel = v.findViewById(R.id.cancel);
        if (Constant.paymentType.equals("Cash"))
            cancel.setVisibility(View.GONE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                MainUI.getLoaderDialog().dismiss();
                //getActivity().finish();
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String inputString = input.getText().toString();

                if (inputString.equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.please_input_all), Toast.LENGTH_SHORT).show();
                    return;
                }
                double b = Double.parseDouble(inputString);
                if (b < tp) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.need_money) + " " + (b - tp), Toast.LENGTH_SHORT).show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("change", Constant.decimalPlaces((b - tp)));
                    bundle.putString("tp", Constant.decimalPlaces(tp));
                    bundle.putString("remaining", Constant.decimalPlaces(b));
                    bundle.putString("price", Constant.decimalPlaces(subTotal));
                    bundle.putInt("order", getArguments().getInt("order"));
                    bundle.putDouble("tax", tax);
                    bundle.putDouble("discount", discount);
                    EndPaymentFragmentDialog newFragment = new EndPaymentFragmentDialog(list);
                    newFragment.setArguments(bundle);
                    newFragment.setMisc(misc);
                    newFragment.show(getFragmentManager(), "");
                    dismiss();
                }

            }
        });
        return v;
    }


}
