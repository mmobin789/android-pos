package com.jouple.posapp.ui.activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.drive.DriveScopes
import com.jouple.posapp.R
import com.jouple.posapp.domain.sale.Sale
import com.jouple.posapp.domain.sale.SaleLedger
import com.jouple.posapp.internet.OnFileUploadListener
import com.jouple.posapp.internet.ReportUploadTask
import com.jouple.posapp.ui.utils.Constant
import kotlinx.android.synthetic.main.activity_refund.*
import org.jetbrains.annotations.Nullable
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

class RefundActivity : BaseActivity(), View.OnClickListener, OnFileUploadListener {


    private var file: File? = null
    private var refundSale: Sale? = null
    private var accountOwnerName: String? = ""
    override fun onFileUploadSuccess(file: com.google.api.services.drive.model.File) {
        Toast.makeText(this, file.id + " upload success", Toast.LENGTH_SHORT).show()
        ok.isEnabled = true
        ok.text = "generate"


    }


    private fun getSaleForRefund(invoiceNo: Int): Boolean {
        val saleLedger = SaleLedger.getInstance()
        val sale = saleLedger.getSaleById(invoiceNo)
        return if (sale != null) {
            val json = JSONObject()
            json.put("id", sale.id)
            json.put("startTime", sale.startTime)
            json.put("endTime", sale.endTime)
            json.put("status", sale.status)
            json.put("totalItemsSold", sale.allLineItem.size)
            val lineItems = sale.allLineItem
            val items = JSONArray()
            for (lineItem in lineItems) {
                val item = JSONObject()

            }

            true
        } else false
    }

    override fun onFileUploadFail(error: String, authIntent: Intent) {
        ok.text = "Authentication Required"
        startActivityForResult(authIntent, 1)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        uploadRefund()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            ok.id -> generateRefundReportToServer(p0)

            else -> {
                showMenu(p0!!)
            }
        }
    }


    private fun showMenu(p0: View) {
        val popupMenu = PopupMenu(p0.context, p0)
        val menu = popupMenu.menu
        menu.add("Cash")
        menu.add("Credit Card")
        popupMenu.setOnMenuItemClickListener {
            val data = "Payment Type: " + it.title
            typeTV.text = data
            true
        }

        popupMenu.show()
    }

    @Nullable
    private fun getFilteredFile(files: Array<File>): File? {

        return files.firstOrNull { it.nameWithoutExtension.contains(etInvoice.text) }
    }

    private fun generateRefundReportToServer(p0: View) {

        val matbInvoices = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), getString(R.string.app_name) + " invoices").listFiles()
        val message = if (matbInvoices == null)
            "No MATB Invoice Available"
        else {
            if (etInvoice.text.isNotEmpty()) {
                file = getFilteredFile(matbInvoices)
                if (file != null) {
                    file!!.nameWithoutExtension


                } else {
                    "No Matching Invoice Found"
                }
            } else {
                "Please Enter an Invoice #"
            }
        }
        Toast.makeText(p0.context, message, Toast.LENGTH_SHORT).show()
        Log.i("file", message)
        if (file != null && !accountOwnerName.isNullOrEmpty()) {

            // uploadRefund()
        }
    }

    private fun uploadRefund() {
        ok.text = "Uploading..."
        ok.isEnabled = false
        val googleAuth = GoogleAccountCredential.usingOAuth2(this, listOf(DriveScopes.DRIVE))
        googleAuth.backOff = ExponentialBackOff()
        googleAuth.selectedAccountName = accountOwnerName
        val uploadTask = ReportUploadTask(ReportUploadTask.FileType.PDF, googleAuth, file!!, this)
        uploadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refund)
        ok.setOnClickListener(this)
        typeTV.setOnClickListener(this)
        accountOwnerName = Constant.getPreferences(this).getString(LoginActivity.PREF_ACCOUNT_NAME, null)
        if (accountOwnerName.isNullOrEmpty()) {
            ok.text = "Google Account Authentication Required Please go to settings and grant access to your google account"
            ok.isEnabled = false
        }
    }
}
