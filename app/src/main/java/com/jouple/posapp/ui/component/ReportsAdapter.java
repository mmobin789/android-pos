package com.jouple.posapp.ui.component;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.sale.QuickLoadSale;
import com.jouple.posapp.domain.sale.Sale;
import com.jouple.posapp.ui.utils.Constant;

import java.util.List;

/**
 * Created by macbook on 13/11/2017.
 */

public class ReportsAdapter extends BaseRecyclerAdapter {
    private List<Sale> list;

    public ReportsAdapter(List<Sale> list) {
        this.list = list;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GeneralViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sales_list, parent, false), GeneralViewHolder.Adapter.reports, listener);
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        Sale sale = list.get(position);
        holder.name.setText(String.valueOf(sale.getId()));
        holder.quantity.setText(sale.getEndTime());
        QuickLoadSale quickLoadSale = (QuickLoadSale) sale;
        holder.price.setText(Constant.decimalPlaces(sale.getTotal()));
        holder.tax.setText(Constant.decimalPlaces(quickLoadSale.getTotalTax()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
