package com.jouple.posapp.ui.inventory;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.techicalservices.DatabaseExecutor;
import com.jouple.posapp.techicalservices.Demo;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.activities.ProductDetailActivity;
import com.jouple.posapp.ui.component.BaseRecyclerAdapter;
import com.jouple.posapp.ui.component.ProductsAdapter;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.utils.AppButton;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.List;

import static com.jouple.posapp.ui.component.PagerAdapter.productsFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.saleFragment;
import static com.jouple.posapp.ui.utils.Constant.isAdmin;

/**
 * UI for Inventory, shows list of Product in the ProductCatalog.
 * Also use for a sale process of adding Product into sale.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class ProductsFragment extends UpdatableFragment {


    //private List<Map<String, String>> inventoryList;
    public AddProductDialogFragment addProductDialogFragment;
    public EditText searchBox;
    // private ListView inventoryListView;
    private RecyclerView inventoryRV;
    private TextView counter;
    private Button scanButton;
    private ProductCatalog productCatalog;
    private AppButton addProductButton, cart;
    private Register register;
    private Resources res;
    private int catID;


    public ProductsFragment() {

    }

    public static ProductsFragment getInstance() {
        if (productsFragment == null)
            productsFragment = new ProductsFragment();
        return (ProductsFragment) productsFragment;
    }


    public void searchProductsByCategoryID(int catID) {
        this.catID = catID;
        //search(false);
    }

    /**
     * Construct a new InventoryFragment.
     * <p>
     * //    * @param saleFragment
     */


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(this.getClass().getName(), "init");
        try {
            productCatalog = Inventory.getInstance().getProductCatalog();
            register = Register.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.layout_inventory, container, false);

        res = getResources();
        //      inventoryListView = view.findViewById(R.id.productListView);
        inventoryRV = view.findViewById(R.id.productRV);
        addProductButton = view.findViewById(R.id.addProductButton);
        scanButton = view.findViewById(R.id.scanButton);
        searchBox = view.findViewById(R.id.searchBox);
        cart = view.findViewById(R.id.showProducts);
        counter = view.findViewById(R.id.counter);
        initUI(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        SleekUI.initRecyclerView(inventoryRV, true);
        AppButton back = view.findViewById(R.id.filter);
        back.setTypeface(SleekUI.fontAwesome(getContext()));
        back.setText(R.string.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert getParentFragment() != null;
                ((MainUI) getParentFragment()).inventory.setCurrentItem(0);
            }
        });


    }


    /**
     * Initiate this UI.
     *
     * @param view
     */
    private void initUI(View view) {

        view.findViewById(R.id.btns).findViewById(R.id.nav2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainActivity().getViewPager().setCurrentItem(2, true);
            }
        });
        cart.setText("Shopping Cart".trim());
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainActivity().getViewPager().setCurrentItem(2, true);
            }
        });

        addProductButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showPopup(getFragmentManager());
            }
        });
        update();

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                search(true);
            }
        });

//        inventoryListView.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                int id = Integer.parseInt(inventoryList.get(i).get("id"));
//                register.addItem(Inventory.getChargedProduct(productCatalog.getProductById(id)), 1);
//                getMainActivity().getViewPager().setCurrentItem(1);
//                saleFragment.update();
//
//            }
//        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(getMainActivity());
                scanIntegrator.initiateScan();
            }
        });

        checkAdmin();

    }

    private void checkAdmin() {
        boolean b;
        b = isAdmin;


        scanButton.setEnabled(b);

    }

//    public void setCounter(String s) {
//        if (counter != null) {
//            counter.setText(s);
//            update();
//        }


    private void showList(final List<Product> list) {

        //    inventoryList = new ArrayList<>();

//        for (Product product : list) {
//            inventoryList.add(product.toMap());
//        }

        ProductsAdapter adapter = new ProductsAdapter(list);
        inventoryRV.setAdapter(adapter);


        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnListClickListener() {
            @Override
            public void OnClick(View v, int position) {

                register.addItem(Inventory.getChargedProduct(list.get(position), v.getContext()), 1);
                saleFragment.update();
            }

            @Override
            public boolean OnLongClick(View v, int position) {
                Intent productDetail = new Intent(v.getContext(), ProductDetailActivity.class);
                productDetail.putExtra("id", list.get(position).getId());
                startActivity(productDetail);
                return true;
            }

        });


//        ButtonAdapter sAdap = new ButtonAdapter(getActivity(), inventoryList,
//                R.layout.listview_inventory, new String[]{"name"}, new int[]{R.id.name}, R.id.optionView, "id");
//        inventoryListView.setAdapter(sAdap);
    }

    /**
     * Search.
     */
    private void search(boolean direct) {
        String search = "";
        if (direct)
            search = searchBox.getText().toString();

        switch (search) {
            case "/demo":
                testAddProduct();
                searchBox.setText("");
                break;
            case "/clear":
                DatabaseExecutor.getInstance().dropAllData();
                searchBox.setText("");
                break;
            case "":
                showList(productCatalog.getAllProduct());
                break;
            default:
                List<Product> result;
                //     if (direct)
                result = productCatalog.searchProduct(search);
                //     else
                //          result = productCatalog.getProdu
                //
                // ctsByCategoryID(catID);
                showList(result);
                if (result.isEmpty()) {


                }
                break;
        }
    }


    public void getBarcodeResult(IntentResult scanningResult) {


        String scanContent = scanningResult.getContents();
        searchBox.setText(scanContent);

    }

    /**
     * Test adding product
     */
    protected void testAddProduct() {
        Demo.testProduct(getActivity());
        Toast.makeText(getActivity(), res.getString(R.string.success),
                Toast.LENGTH_SHORT).show();
    }


    public void showPopup(FragmentManager fragmentManager) {
        addProductDialogFragment = new AddProductDialogFragment();
        addProductDialogFragment.show(fragmentManager, "");

    }

    @Override
    public void update() {
        search(false);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        update();
//    }

}