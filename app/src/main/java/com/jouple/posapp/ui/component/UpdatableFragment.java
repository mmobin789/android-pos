package com.jouple.posapp.ui.component;

import android.support.v4.app.Fragment;

import com.jouple.posapp.ui.activities.MainActivity;

/**
 * Fragment which is able to call update() from other class.
 * This is used by Delegation pattern.
 *
 * @author Refresh Team
 */
public abstract class UpdatableFragment extends Fragment {


    /**
     * Update fragment.
     */
    public abstract void update();


    public MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
