package com.jouple.posapp.ui.sale;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.DateTimeStrategy;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.activities.PaymentDetailActivity;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.ArrayList;
import java.util.List;

import static com.jouple.posapp.ui.component.PagerAdapter.reportFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.saleFragment;


/**
 * A dialog shows the total change and confirmation for Sale.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class EndPaymentFragmentDialog extends DialogFragment {

    private Register regis;
    private String tax;
    private String discount;
    private String tp, change;
    private List<LineItem> list;
    private Product misc;

    public EndPaymentFragmentDialog(List<LineItem> list) {
        this.list = list;


    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            regis = Register.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.dialog_paymentsuccession, container, false);

        change = getArguments().getString("change");
        tax = Constant.decimalPlaces(getArguments().getDouble("tax"));
        discount = Constant.decimalPlaces(getArguments().getDouble("discount"));
        tp = getArguments().getString("tp");
        TextView chg = v.findViewById(R.id.changeTxt);
        SleekUI.setHeader(getString(R.string.change), v);
        chg.setText(change);
        Button doneButton = v.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                end();
            }
        });

        return v;
    }


    /**
     * End
     */
    private void end() {

        regis.endSale(DateTimeStrategy.getCurrentTime());
        saleFragment.update();
        reportFragment.update();
        //runInBackground();
        // printUsingSystem();
        showPaymentDetail();
        dismiss();


        // assert getActivity() != null;
        // getActivity().finish();


    }


    private void showPaymentDetail() {

        Intent intent = new Intent(getContext(), PaymentDetailActivity.class);
        intent.putExtra("tp", tp);
        intent.putExtra("tax", tax);
        intent.putExtra("change", change);
        intent.putExtra("discount", discount);
        intent.putExtra("misc", misc);
        intent.putExtra("order", getArguments().getInt("order"));
        intent.putExtra("remaining", getArguments().getString("remaining"));
        intent.putExtra("price", getArguments().getString("price"));
        intent.putParcelableArrayListExtra("lineItems", (ArrayList<? extends Parcelable>) list);
        assert getActivity() != null;
        getActivity().finish();
        startActivity(intent);
    }

    public void setMisc(Product misc) {
        this.misc = misc;
    }
}
