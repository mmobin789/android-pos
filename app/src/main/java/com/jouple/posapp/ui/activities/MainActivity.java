package com.jouple.posapp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.component.UpdatableFragment;
import com.jouple.posapp.ui.inventory.CategoryFragment;
import com.jouple.posapp.ui.inventory.MainUI;
import com.jouple.posapp.ui.inventory.ProductsFragment;
import com.jouple.posapp.ui.sale.ReportFragment;
import com.jouple.posapp.ui.utils.Constant;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import static com.jouple.posapp.ui.component.PagerAdapter.categoryFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.mainUI;
import static com.jouple.posapp.ui.component.PagerAdapter.productsFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.reportFragment;
import static com.jouple.posapp.ui.utils.Constant.setLanguage;

/**
 * This UI loads 3 main pages (Inventory, Sale, Report)
 * Makes the UI flow by slide through pages using ViewPager.
 *
 * @author Refresh Team
 */
@SuppressLint("NewApi")
public class MainActivity extends BaseActivity {


    public static boolean showInventoryUIOnly;
    private static boolean SDK_SUPPORTED;
    public ImagePicker imagePicker;

    private ViewPager viewPager;
    private ProductCatalog productCatalog;
    private String productId;
    private Product product;
    private PagerAdapter pagerAdapter;
    private Resources res;

    @SuppressLint("NewApi")
    /**
     * Initiate this UI.
     */
    private void initiateActionBar() {
        if (SDK_SUPPORTED) {
            ActionBar actionBar = getActionBar();

            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            ActionBar.TabListener tabListener = new ActionBar.TabListener() {
                @Override
                public void onTabReselected(Tab tab, FragmentTransaction ft) {
                }

                @Override
                public void onTabSelected(Tab tab, FragmentTransaction ft) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(Tab tab, FragmentTransaction ft) {
                }
            };
            actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.category))
                    .setTabListener(tabListener), 0, false);
            actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.inventory))
                    .setTabListener(tabListener), 1, false);
            actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.sale))
                    .setTabListener(tabListener), 2, true);
            actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.report))
                    .setTabListener(tabListener), 3, false);

            actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#73bde5")));

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   Thread.setDefaultUncaughtExceptionHandler(new ExceptionDealer());
        Constant.initiateCoreApp(this);
        res = getResources();
        setContentView(R.layout.layout_main);
        viewPager = findViewById(R.id.pager);
        int pageNo = getIntent().getIntExtra("pageNo", -1);
        showInventoryUIOnly = getIntent().getBooleanExtra("ui", false);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), res);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(pageNo);

        //SDK_SUPPORTED = false;
        /// initiateActionBar();


        //viewPager.addOnPageChangeListener(this);
    }


    /**
     * Open quit dialog.
     */
    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                MainActivity.this);
        quitDialog.setTitle(res.getString(R.string.dialog_quit));
        quitDialog.setPositiveButton(res.getString(R.string.quit), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        quitDialog.setNegativeButton(res.getString(R.string.no), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        quitDialog.show();
    }

    /**
     * Option on-click handler.
     *
     * @param view
     */
    public void optionOnClickHandler(View view) {
        productId = view.getTag().toString();
        try {
            productCatalog = Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }
        product = productCatalog.getProductById(Integer.parseInt(productId));
        openDetailDialog();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        imagePicker.handlePermission(requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (imagePicker != null)
            imagePicker.handleActivityResult(resultCode, requestCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, data);
        if (scanningResult != null) {
            ((CategoryFragment) categoryFragment).getBarcodeResult(scanningResult);
            ((ProductsFragment) productsFragment).getBarcodeResult(scanningResult);
        }


    }

    public void initImagePicker(OnImagePickedListener listener) {
        if (imagePicker == null)
            imagePicker = new ImagePicker(this, productsFragment, listener);

        imagePicker.choosePicture(false);


    }

    /**
     * Open detail dialog.
     */
    private void openDetailDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(MainActivity.this);
        quitDialog.setTitle(product.getName());
        quitDialog.setPositiveButton(res.getString(R.string.remove), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                openRemoveDialog();
            }
        });

        quitDialog.setNegativeButton(res.getString(R.string.product_detail), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent newActivity = new Intent(MainActivity.this,
                        ProductDetailActivity.class);
                newActivity.putExtra("id", productId);
                startActivityForResult(newActivity, 2);
            }
        });

        quitDialog.show();
    }


    /**
     * Open remove dialog.
     */
    private void openRemoveDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                MainActivity.this);
        quitDialog.setTitle(res.getString(R.string.dialog_remove_product));
        quitDialog.setPositiveButton(res.getString(R.string.no), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        quitDialog.setNegativeButton(res.getString(R.string.remove), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                productCatalog.suspendProduct(product);
                pagerAdapter.update(0);
            }
        });

        quitDialog.show();
    }

    /**
     * Get view-pager
     *
     * @return
     */
    public ViewPager getViewPager() {
        return viewPager;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.lang_en:
                setLanguage(this, "en", false);
                return true;
            case R.id.lang_th:
                setLanguage(this, "th", false);
                return true;
            case R.id.lang_jp:
                setLanguage(this, "jp", false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void addMoreItems(View v) {
        viewPager.setCurrentItem(1, true);
    }


    /**
     * @author Refresh team
     */
    private class PagerAdapter extends FragmentStatePagerAdapter {

        private UpdatableFragment[] fragments;
        private String[] fragmentNames;

        /**
         * Construct a new PagerAdapter.
         *
         * @param fragmentManager
         * @param res
         */
        PagerAdapter(FragmentManager fragmentManager, Resources res) {

            super(fragmentManager);

            reportFragment = new ReportFragment();
            mainUI = new MainUI();

//            saleFragment = new SaleFragment();
//            productsFragment = new ProductsFragment();
            //  UpdatableFragment categoryFragment = new CategoryFragment();

            fragments = new UpdatableFragment[]{mainUI,
                    reportFragment};
            fragmentNames = new String[]{res.getString(R.string.category), res.getString(R.string.inventory),
                    res.getString(R.string.cart),
                    res.getString(R.string.report)};


        }

        @Override
        public Fragment getItem(int i) {
            return fragments[i];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            return fragmentNames[i];
        }

        /**
         * Update
         *
         * @param index
         */
        public void update(int index) {
            fragments[index].update();
        }

    }
}