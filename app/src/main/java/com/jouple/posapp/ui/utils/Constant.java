package com.jouple.posapp.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.jouple.posapp.domain.DateTimeStrategy;
import com.jouple.posapp.domain.LanguageController;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.sale.Admin;
import com.jouple.posapp.domain.sale.AdminControls;
import com.jouple.posapp.domain.sale.AdminDAO;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.domain.sale.SaleLedger;
import com.jouple.posapp.techicalservices.AndroidDatabase;
import com.jouple.posapp.techicalservices.Database;
import com.jouple.posapp.techicalservices.DatabaseExecutor;
import com.jouple.posapp.techicalservices.inventory.InventoryDao;
import com.jouple.posapp.techicalservices.inventory.InventoryDaoAndroid;
import com.jouple.posapp.techicalservices.sale.SaleDao;
import com.jouple.posapp.techicalservices.sale.SaleDaoAndroid;

import java.util.Locale;

/**
 * Created by macbook on 17/11/2017.
 */

public class Constant {
    public static boolean isAdmin = false;
    public static String paymentType = "";
    private static Admin admin;
    private static SharedPreferences preferences;
    private static String key_vat = "vat", key_tab = "tab", key_admin = "admin", key_password = "password", key_tax = "tax", key_discount = "discount", key_vendor = "vendor", key_code = "code";

    private Constant() {

    }

    public static void saveActivationCode(Context context, String code) {
        initPrefs(context);
        preferences.edit().putString(key_code, code).apply();
    }

    public static String getActivationCode(Context context) {
        initPrefs(context);
        return preferences.getString(key_code, null);
    }


    public static SharedPreferences getPreferences(Context context) {
        initPrefs(context);
        return preferences;
    }

    public static String decimalPlaces(double d) {

        return String.format(Locale.ENGLISH, "%.2f", d);
    }

    public static Admin getAdmin(Context context) {
        if (admin == null)
            checkAdminControl(context);
        return admin;
    }

//    public static double getTax(double price, double tax) {
//        double val = 1 + (tax / 100);
//        return price * val;
//    }
//
//    public static double getDiscount(double price, double discount) {
//        double val = 1 - (discount / 100);
//        return price * val;
//    }

    public static double getPercentageValue(double price, double percent) {
        return price * percent / 100;
    }

    public static void clearAdmin(Context context) {
        initPrefs(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key_admin);
        editor.remove(key_password);
        editor.apply();


    }

    public static String getLastInvoice(Context context) {
        initPrefs(context);
        return preferences.getString("invoice", "Not Found");
    }

    public static void saveLastInvoice(Context context, String filePath) {
        initPrefs(context);
        preferences.edit().putString("invoice", filePath).apply();
    }

    private static void initPrefs(Context context) {
        if (preferences == null)
            preferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public static boolean setGeneralSettings(Context context, Admin admin) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key_tax, admin.getTax() + "");
        editor.putString(key_discount, admin.getDiscount() + "");
        editor.putString(key_tab, admin.getTab());
        editor.putString(key_vendor, admin.getVendor());
        editor.putInt(key_vat, admin.getVatNumber());
        editor.apply();
        return checkAdminControl(context);
    }

    public static void initiateCoreApp(Context context) {
        Database database = new AndroidDatabase(context);
        InventoryDao inventoryDao = new InventoryDaoAndroid(database);
        SaleDao saleDao = new SaleDaoAndroid(database);
        DatabaseExecutor.setDatabase(database);
        LanguageController.setDatabase(database);
        AdminDAO adminDAO = new AdminControls(database);
        Admin.setAdminDAO(adminDAO);
        Inventory.setInventoryDao(inventoryDao);
        Register.setSaleDao(saleDao);
        SaleLedger.setSaleDao(saleDao);

        DateTimeStrategy.setLocale("th", "TH");
        setLanguage(context, LanguageController.getInstance().getLanguage(), true);
        Constant.checkAdminControl(context);
        Log.d("Core App", "INITIATE");


    }

    /**
     * Set language
     *
     * @param localeString
     * @param firstTime
     */
    public static void setLanguage(Context context, String localeString, boolean firstTime) {
        Locale locale = new Locale(localeString);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        LanguageController.getInstance().setLanguage(localeString);
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
//        if (!firstTime) {
//            Intent intent = context.getIntent();
//            finish();
//            startActivity(intent);
//        }
    }


    public static void saveAdmin(Context context, Admin admin) {
        initPrefs(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key_admin, admin.getName());
        editor.putString(key_password, admin.getPassword());
        editor.putString(key_vendor, admin.getVendor());
        editor.apply();
        checkAdminControl(context);
    }

    public static boolean checkAdminControl(Context context) {
        initPrefs(context);
        String name = preferences.getString(key_admin, "Not Logged In");
        String password = preferences.getString(key_password, null);
        String taxS = preferences.getString(key_tax, null);
        String dS = preferences.getString(key_discount, null);
        String tab = preferences.getString(key_tab, "");
        String vendor = preferences.getString(key_vendor, null);
        int vat = preferences.getInt(key_vat, 0);
        double tax = 0;
        double discount = 0;
        if (!TextUtils.isEmpty(taxS))
            tax = Double.parseDouble(taxS);
        if (!TextUtils.isEmpty(dS))
            discount = Double.parseDouble(dS);
        isAdmin = !TextUtils.isEmpty(name) && !TextUtils.isEmpty(password);
        if (isAdmin) {
            admin = new Admin(name, vendor, password);

        } else {
            admin = Admin.getAdminSettings();
        }
        admin.setName(name);
        admin.setTax(tax);
        admin.setDiscount(discount);
        admin.setTab(tab);
        admin.setVendor(vendor);
        admin.setVatNumber(vat);
        return isAdmin;

    }
}
