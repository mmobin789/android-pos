package com.jouple.posapp.ui.component;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.utils.Constant;

import java.util.List;

/**
 * Created by macbook on 09/11/2017.
 */

public class SalesAdapter extends BaseRecyclerAdapter {
    private List<LineItem> list;
    private boolean isPrinting;
    private boolean isReportDetailUI;
    private ProductCatalog productCatalog;

    public SalesAdapter(List<LineItem> list) {
        this.list = list;
        try {
            productCatalog = Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }
    }

    public void setReportDetailUI(boolean reportDetailUI) {
        isReportDetailUI = reportDetailUI;
    }

    public void setPrintingUI(boolean b) {
        isPrinting = b;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        GeneralViewHolder holder = new GeneralViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sales_list, parent, false), GeneralViewHolder.Adapter.sales, listener);
        if (isPrinting) {
            holder.iv.setVisibility(View.GONE);
        }
        if (isReportDetailUI) {
            holder.iv.setVisibility(View.GONE);
            holder.tax.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        LineItem lineItem = list.get(position);
        Product product = lineItem.getProduct();

        holder.setOnPicasso(product.getImage());
        holder.name.setText(product.getName());
        holder.quantity.setText(String.valueOf(lineItem.getQuantity()));
        holder.tax.setText(String.valueOf(product.getTax() * lineItem.getQuantity()));
        holder.discount.setText(Constant.decimalPlaces(product.getDiscount() * lineItem.getQuantity()));
        holder.price.setText(Constant.decimalPlaces(lineItem.getPriceAtSale() * lineItem.getQuantity()));
        holder.productCategory.setText(productCatalog.getCategoryByID(product.getCategoryID()).getName());
        if (product.getName().equalsIgnoreCase("misc"))
            holder.productCategory.setText("misc");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
