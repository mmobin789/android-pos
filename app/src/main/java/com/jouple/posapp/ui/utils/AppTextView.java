package com.jouple.posapp.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.jouple.posapp.R;

/**
 * Created by macbook on 09/11/2017.
 */

public class AppTextView extends AppCompatTextView {


    public AppTextView(Context context) {
        super(context);
    }

    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.AppTextView, 0, 0);
        boolean bold = a.getBoolean(R.styleable.AppTextView_bold, false);
        boolean enableDefaultTextColor = a.getBoolean(R.styleable.AppTextView_setDefaultTextColorEnabled, true);
        boolean enableDefaultTextSize = a.getBoolean(R.styleable.AppTextView_setDefaultTextSizeEnabled, false);
        setDefaultTextColorEnabled(enableDefaultTextColor);
        setBoldTextEnabled(bold);
        setDefaultTextSizeEnabled(enableDefaultTextSize);
        a.recycle();

    }

    public void setDefaultTextColorEnabled(boolean enabled) {
        if (enabled)
            setTextColor(ContextCompat.getColor(getContext(), R.color.black));

    }

    public void setBoldTextEnabled(boolean enabled) {

        if (enabled) {
            setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
        } else {
            setTypeface(Typeface.SANS_SERIF);
        }

    }

    public void setDefaultTextSizeEnabled(boolean enabled) {
        if (enabled)
            setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
    }
}
