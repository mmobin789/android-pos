package com.jouple.posapp.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.jouple.posapp.R;
import com.jouple.posapp.api.ActivationCode;
import com.jouple.posapp.api.ApiCalls;
import com.jouple.posapp.api.OnActivationListener;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

import org.jetbrains.annotations.NotNull;

import io.fabric.sdk.android.Fabric;

public class HomeActivity extends BaseActivity implements OnActivationListener {
    ImageView logOutBtn;
    LinearLayout admin;


    private void main() {

        admin = findViewById(R.id.admin);
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.isAdmin)
                    dashboardUI(view);
                else
                    loginUI(view);

            }
        });

        findViewById(R.id.pos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.clearAdmin(view.getContext());
                checkAdmin();
                startActivity(new Intent(view.getContext(), MainActivity.class));
            }
        });

        logOutBtn = findViewById(R.id.logout);
        //logOutBtn.setTypeface(SleekUI.logoFont(3, this));
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut(view);


            }
        });

        checkAdmin();
    }

    private void showActivationCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activation_code);
        SleekUI.setHeader("POS Activation", dialog, false);
        dialog.show();
        dialog.findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        final EditText input = dialog.findViewById(R.id.code);
        final Button ok = dialog.findViewById(R.id.confirmButton);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = input.getText().toString();
                if (TextUtils.isEmpty(code))
                    Toast.makeText(view.getContext(), "Activation Code Required", Toast.LENGTH_SHORT).show();
                else {
                    ok.setText("hmm...");
                    ApiCalls.Companion.activationCode(code, HomeActivity.this, ok, dialog);
                }
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_home_new_2);
        Constant.initiateCoreApp(this);
        main();
        if (TextUtils.isEmpty(Constant.getActivationCode(this)))
            showActivationCodeDialog();

    }


    private void checkAdmin() {
        Constant.checkAdminControl(this);
        if (Constant.isAdmin) {
            logOutBtn.setVisibility(View.VISIBLE);
            //admin.setEnabled(false);

        } else {
            logOutBtn.setVisibility(View.GONE);
            // admin.setEnabled(true);
        }

    }

    public void loginUI(View v) {
        startActivityForResult(new Intent(v.getContext(), LoginActivity.class), 3);
    }

    private void dashboardUI(View view) {
        startActivityForResult(new Intent(view.getContext(), DashBoardActivity.class), 3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3 && resultCode == RESULT_OK)
            checkAdmin();
    }

    private void logOut(View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v);
        Menu menu = popup.getMenu();
        //   menu.add(Menu.NONE, 1, Menu.NONE, "Settings");
        menu.add(Menu.NONE, 0, Menu.NONE, "Log Out");


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 0:
                        Constant.clearAdmin(HomeActivity.this);
                        checkAdmin();
                        Toast.makeText(logOutBtn.getContext(), "Logout success", Toast.LENGTH_SHORT).show();
                        break;
//                    case 1:
//                        openGeneralSettingsDialog(HomeActivity.this);
//                        break;
                }
                return true;
            }
        });

//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
    }

    @Override
    public void onActivation(@NotNull ActivationCode activationCode, @NotNull Dialog dialogToBeDismissed) {
        if (activationCode.isVerified()) {
            dialogToBeDismissed.dismiss();
            Constant.saveActivationCode(this, activationCode.getActivationCode());
        } else {
            final Button ok = dialogToBeDismissed.findViewById(R.id.confirmButton);
            ok.setText("INVALID");
            ok.setEnabled(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ok.setText(R.string.ok);
                    ok.setEnabled(true);
                }
            }, 5000);
        }

    }
}
