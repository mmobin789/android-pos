package com.jouple.posapp.ui.utils;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.DateTimeStrategy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by macbook on 03/11/2017.
 */

public class SleekUI {
    private static SleekUI sleekUI;

    private SleekUI() {
    }

    public static SleekUI getInstance() {
        if (sleekUI == null)
            sleekUI = new SleekUI();

        return sleekUI;
    }

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(100)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getActiveInternet(Context context) {
        String data;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null)
            data = activeNetInfo.getTypeName();
        else data = "";
        if (data.isEmpty())
            Toast.makeText(context, "No Internet Available", Toast.LENGTH_LONG).show();
        return data;
    }

    public static Typeface fontAwesome(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fontawesome.ttf");
    }

    public static void slideViewDOWN(final View v) {

        v.setAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.slide_down));

        v.animate().setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();

    }

    public static void slideViewUP(final View v) {
        v.setAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.slide_up));
        v.animate().setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                v.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }

    public static File getImageFile(Context context, Bitmap result) {
        File f = null;
        Bitmap resized = getResizedBitmap(result, 500);
        if (result != null) {
            f = getFileFromBitmap(context, resized);
            Log.i("imageSize", f.length() / 1024 + "kbs");
        }
        return f;
    }

    public static String getRealPathFromURI(Uri uri, Context context) {
        if (uri == null) {
            return null;
        } else {
            String[] projection = new String[]{"_data"};
            Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow("_data");
                cursor.moveToFirst();
                String path = cursor.getString(column_index);
                cursor.close();
                return path;
            } else {
                return uri.getPath();
            }
        }
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage, String imageName) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, imageName, null);
        return Uri.parse(path);
    }

    public static File getFileFromBitmap(Context context, Bitmap bitmap) {
        //create a file to write bitmap data
        File f = new File(context.getCacheDir(), context.getString(R.string.app_name) + DateTimeStrategy.getCurrentTime());
//Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos;
        try {
            f.createNewFile();
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    public static File getResizedFile(Context context, File file) {

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
        bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
        return getFileFromBitmap(context, bitmap);
    }

    private static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static void setHeader(String header, Dialog dialog, boolean showUnderline) {
        TextView
                textView = dialog.findViewById(R.id.header);
        textView.setText(header);
        View line = dialog.findViewById(R.id.line);
        if (!showUnderline)
            line.setVisibility(View.GONE);

    }

    public static void setHeader(String header, View view) {
        TextView
                textView = view.findViewById(R.id.header);
        textView.setText(header);
    }

    public static void setHeader(String header, Activity activity) {
        TextView
                textView = activity.findViewById(R.id.header);
        textView.setText(header);
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        cursor.close();
        return cursor.getString(idx);
    }

    public static void initRecyclerView(RecyclerView recyclerView, boolean vertical) {
        int i;
        if (vertical)
            i = LinearLayoutManager.VERTICAL;
        else
            i = LinearLayoutManager.HORIZONTAL;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), i, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


    }

    public static Typeface logoFont(int i, Context context) {
        String font = "";
        switch (i) {
            case 0:
                font = "amin_hurgada.ttf";
                break;
            case 1:
                font = "phraell_demo.ttf";
                break;
            case 2:
                font = "prototype.ttf";
                break;
            case 3:
                font = "timeburnerbold.ttf";
                break;
            default:
                font = "timeburnernormal.ttf";
                break;
        }
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static TextView getLabelView(Activity activity, int id) {
        return activity.findViewById(id);
    }

    public static TextView getLabelView(View v, int id) {
        return v.findViewById(id);
    }

    public static void updateUI(final TextView textView, String txt, final CharSequence label) {
        textView.setText(txt.toUpperCase());
        textView.setEnabled(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.setText(label.toString().toUpperCase());
                textView.setEnabled(true);

            }
        }, 5000);
    }


}
