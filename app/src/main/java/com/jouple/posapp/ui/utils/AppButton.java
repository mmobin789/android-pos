package com.jouple.posapp.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.jouple.posapp.R;

/**
 * Created by macbook on 17/11/2017.
 */

public class AppButton extends android.support.v7.widget.AppCompatButton {
    public AppButton(Context context) {
        super(context);
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        // setTypeface(SleekUI.logoFont(3, context));

        //   setMinimumHeight(context.getResources().getDimensionPixelOffset(R.dimen._40sdp));

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.AppTextView, 0, 0);
        boolean enableDefaultTextColor = a.getBoolean(R.styleable.AppTextView_setDefaultTextColorEnabled, true);
        boolean enableDefaultTextSize = a.getBoolean(R.styleable.AppTextView_setDefaultTextSizeEnabled, true);
        boolean bold = a.getBoolean(R.styleable.AppTextView_bold, false);
        boolean background = a.getBoolean(R.styleable.AppTextView_setDefaultBackgroundEnabled, true);
        setDefaultBackgroundEnabled(background);
        setBoldTextEnabled(bold);
        setDefaultTextSizeEnabled(enableDefaultTextSize);
        setDefaultTextColorEnabled(enableDefaultTextColor);

        a.recycle();
    }

    public void setDefaultBackgroundEnabled(boolean enabled) {
        if (enabled)
            setBackgroundResource(R.drawable.btn_app);
    }

    public void setBoldTextEnabled(boolean enabled) {

        if (enabled) {
            setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
        } else {
            setTypeface(Typeface.SANS_SERIF);
        }

    }

    public void setDefaultTextColorEnabled(boolean enabled) {
        if (enabled)
            setTextColor(ContextCompat.getColor(getContext(), R.color.white));

    }

    public void setDefaultTextSizeEnabled(boolean enabled) {
        if (enabled)
            setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
    }
}
