package com.jouple.posapp.ui.utils

import android.devkit.api.SerialPort
import android.util.Log
import java.io.IOException


/**
 * Created by macbook on 11/01/2018.
 */
class PrinterReaderThread : Thread() {

    private val configCom = "/dev/ttyVK1"
    private var mSerialPort: SerialPort? = null

    companion object {
        private var printerData: ByteArray? = null
        fun sendMail(message: String, error: Boolean) {
            val s = if (error)
                "POS Printer Error-s"
            else
                "POS Printer"
            Log.e(s, message)

        }
    }


    init {

        try {
            mSerialPort = SerialPort(configCom, 115200, 0)
            sendMail(mSerialPort!!.allDevices.size.toString() + " Device connected", false)
        } catch (e: SecurityException) {
            e.printStackTrace()
            sendMail(Log.getStackTraceString(e), true)
        }


    }

    fun sendCommand(byteArray: ByteArray) {
        try {
            printerData = ByteArray(0)
            mSerialPort?.outputStream?.write(byteArray)

            Log.i("Send: ", HexDump.dumpHex(byteArray))
            sendMail("File size of pdf invoice in bytes array to be printed is " + byteArray.size.toString() + " on Send command", false)

        } catch (e: InterruptedException) {
            e.printStackTrace()
            sendMail(Log.getStackTraceString(e), true)
        } catch (e: IOException) {
            e.printStackTrace()
            sendMail(Log.getStackTraceString(e), true)
        }
    }

    override fun run() {
        while (!isInterrupted) {
            if (mSerialPort?.inputStream == null) {
                Log.e(javaClass.simpleName, "No input Stream")
                sendMail("No Input Found for Printer", false)
                return
            }
            val buffer = ByteArray(65536)
            var size = 0
            try {
                while (size == 0)
                    size = mSerialPort!!.inputStream.available()
                // sendMail("input stream available" + size, false)
                size = mSerialPort!!.inputStream.read(buffer)
                if (size > 0) {
                    val receive = ByteArray(size)
                    for (i in 0 until size)
                        receive[i] = buffer[i]
                    printerData = ArrayUtil.MergerArray(printerData, receive)
                    sendMail(printerData!!.size.toString() + " Printer Data", false)
                    //   timeH.removeCallbacksAndMessages(null)
                    // timehandler.postDelayed(timerunnable, 300); }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                sendMail(Log.getStackTraceString(e), true)
            }
        }


    }
}
