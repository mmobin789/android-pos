package com.jouple.posapp.ui.component;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.ui.utils.Constant;

import java.util.List;

/**
 * Created by macbook on 08/11/2017.
 */

public class ProductsAdapter extends BaseRecyclerAdapter {

    private List<Product> list;

    public ProductsAdapter(List<Product> list) {
        this.list = list;

    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GeneralViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_list, parent, false), GeneralViewHolder.Adapter.products, listener);
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        Product product = list.get(position);
        holder.setOnPicasso(product.getImage());
        holder.name.setText(product.getName());
        holder.price.setText(Constant.decimalPlaces(product.getUnitPrice()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
