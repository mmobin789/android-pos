package com.jouple.posapp.ui.activities

import android.Manifest
import android.accounts.AccountManager
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.drive.DriveScopes
import com.jouple.posapp.R
import com.jouple.posapp.internet.ReportSync
import com.jouple.posapp.ui.utils.Constant
import com.jouple.posapp.ui.utils.SleekUI
import kotlinx.android.synthetic.main.activity_settings.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

class SettingsActivity : BaseActivity() {
    companion object {
        val PREF_ACCOUNT_NAME = "accountName"
    }

    private val REQUEST_ACCOUNT_PICKER = 1000
    private val REQUEST_AUTHORIZATION = 1001
    private val REQUEST_GOOGLE_PLAY_SERVICES = 1002
    private var mCredential: GoogleAccountCredential? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val admin = Constant.getAdmin(this)
        tab.setText(admin.tab)
        vat.setText(admin.vatNumber.toString())
        ok.setOnClickListener {
            save(it)
        }
        initGoogleDriveApi()


    }

    private fun reportSync() {
        //        if (!Constant.isReportSyncEnabled(this))
        //            Toast.makeText(this, "Report Sync Feature will require permission here on first upload", Toast.LENGTH_LONG).show();
        val reportSync = Intent(this, ReportSync::class.java)
        if (SleekUI.isServiceRunning(ReportSync::class.java, this)) {
            stopService(reportSync)

        }

        startService(reportSync)
    }

    fun sendEmail(v: View) {
        val invoicePDF = File(Constant.getLastInvoice(v.context))

        if (!invoicePDF.exists()) {
            Toast.makeText(v.context, "No Invoice Found", Toast.LENGTH_LONG).show()
        } else {
            val email = Intent(Intent.ACTION_SEND)
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //   email.setType("text/plain");
            email.putExtra(Intent.EXTRA_SUBJECT, invoicePDF.name)
            // email.putExtra(Intent.EXTRA_TEXT, mBodyEditText.getText().toString());
            val fileUri = FileProvider.getUriForFile(this, packageName + ".ui.utils.PosFileProvider", invoicePDF)
            email.putExtra(Intent.EXTRA_STREAM, fileUri)
            //  email.setType("message/rfc822");
            email.type = "vnd.android.cursor.dir/email"
            startActivity(email)
        }

    }

    private fun save(view: View) {
        val admin = Constant.getAdmin(view.context)
        val tab = tab.text.toString()
        val vat = vat.text.toString()
        if (tab.isNotEmpty() || vat.isNotEmpty()) {
            admin.tab = tab
            admin.vatNumber = vat.toInt()
            Constant.setGeneralSettings(view.context, admin)
            Toast.makeText(view.context, "success", Toast.LENGTH_SHORT).show()
        }


    }

    private fun initGoogleDriveApi() {
        mCredential = GoogleAccountCredential.usingOAuth2(
                applicationContext, listOf(DriveScopes.DRIVE))
                .setBackOff(ExponentialBackOff())
        getResultsFromApi()
    }

    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private fun getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices()
        } else if (mCredential?.selectedAccountName == null) {
            chooseAccount()
        } else if (SleekUI.getActiveInternet(this) == null) {
            Toast.makeText(this, "No network connection available.", Toast.LENGTH_SHORT).show()
        } else {
            Log.i("GoogleDriveAPI", "Set for User " + mCredential?.selectedAccountName)
            reportSync()

        }
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private fun isGooglePlayServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this)
        return connectionStatusCode == ConnectionResult.SUCCESS
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private fun acquireGooglePlayServices() {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     * Google Play Services on this device.
     */
    private fun showGooglePlayServicesAvailabilityErrorDialog(
            connectionStatusCode: Int) {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val dialog = apiAvailability.getErrorDialog(
                this,
                connectionStatusCode,

                REQUEST_GOOGLE_PLAY_SERVICES)
        dialog.show()
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(1003)
    private fun chooseAccount() {
        if (EasyPermissions.hasPermissions(
                        this, Manifest.permission.GET_ACCOUNTS)) {
            val accountName = Constant.getPreferences(this).getString(PREF_ACCOUNT_NAME, null)
            if (accountName != null) {
                mCredential?.selectedAccountName = accountName
                getResultsFromApi()
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential?.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER)
            }
        }
    }


    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     *
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode  code indicating the result of the incoming
     * activity result.
     * @param data        Intent (containing result data) returned by incoming
     * activity result.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GOOGLE_PLAY_SERVICES -> if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this,
                        "This app requires Google Play Services. Please install " + "Google Play Services on your device and relaunch this app.", Toast.LENGTH_LONG).show()
            } else {
                getResultsFromApi()
            }
            REQUEST_ACCOUNT_PICKER -> if (resultCode == Activity.RESULT_OK && data != null &&
                    data.extras != null) {
                val accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
                if (accountName != null) {
                    val editor = Constant.getPreferences(this).edit()
                    editor.putString(PREF_ACCOUNT_NAME, accountName)
                    editor.apply()
                    mCredential?.selectedAccountName = accountName
                    getResultsFromApi()
                }
            }
            REQUEST_AUTHORIZATION -> if (resultCode == Activity.RESULT_OK) {
                getResultsFromApi()
            }
        }
    }


}
