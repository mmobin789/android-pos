package com.jouple.posapp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.DateTimeStrategy;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.domain.inventory.ProductLot;
import com.jouple.posapp.domain.inventory.Stock;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.component.StockAdapter;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.util.List;

/**
 * UI for shows the datails of each Product.
 *
 * @author Refresh Team
 */
@SuppressLint("NewApi")
public class ProductDetailActivity extends BaseActivity {

    private ProductCatalog productCatalog;
    private Stock stock;
    private Product product;
    //private List<Map<String, String>> stockList;
    private EditText nameBox, descBox, imageBox;
    private EditText barcodeBox;
    private TextView stockSumBox;
    private EditText priceBox;
    private Button addProductLotButton;
    private Button submitEditButton;
    private Button cancelEditButton;
    private Button openEditButton;
    private RecyclerView stockListView;
    private int id;
    private String[] remember;
    private AlertDialog.Builder popDialog;
    private LayoutInflater inflater;
    private Resources res;
    private EditText costBox;
    private EditText quantityBox, taxBox, discountBox;
    private Button confirmButton;
    private Button clearButton;
    private View Viewlayout;
    private ImagePicker imagePicker;
    private AlertDialog alert;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return true;
    }


    @SuppressLint("NewApi")
    private void initiateActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(res.getString(R.string.product_detail));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33B5E5")));
    }

    public void deleteProduct(View v) {
        String s = "";
        if (productCatalog.removeProduct(product)) {
            s = "success";
            onBackPressed();
        } else s = "failed";
        Toast.makeText(v.getContext(), s, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        //initiateActionBar();

        try {
            stock = Inventory.getInstance().getStock();
            productCatalog = Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        id = getIntent().getIntExtra("id", -1);
        product = productCatalog.getProductById(id);

        initUI();
        remember = new String[5];

        nameBox.setText(product.getName());
        priceBox.setText("" + product.getUnitPrice() + "");
        descBox.setText(product.getDescription());
        imageBox.setText(product.getImage());
        barcodeBox.setText(product.getBarcode());
        taxBox.setText("" + product.getTax() + "");
        discountBox.setText("" + product.getDiscount() + "");

    }


    private void initUI() {
        setContentView(R.layout.layout_productdetail_main);
        stockListView = findViewById(R.id.stockListView);
        SleekUI.initRecyclerView(stockListView, true);
        nameBox = findViewById(R.id.nameBox);
        descBox = findViewById(R.id.desc);
        imageBox = findViewById(R.id.image);
        priceBox = findViewById(R.id.priceBox);
        taxBox = findViewById(R.id.taxBox);
        discountBox = findViewById(R.id.discountBox);
        barcodeBox = findViewById(R.id.barcodeBox);
        stockSumBox = findViewById(R.id.stockSumBox);
        submitEditButton = findViewById(R.id.submitEditButton);
        submitEditButton.setVisibility(View.INVISIBLE);
        cancelEditButton = findViewById(R.id.cancelEditButton);
        cancelEditButton.setVisibility(View.INVISIBLE);
        openEditButton = findViewById(R.id.openEditButton);
        openEditButton.setVisibility(View.VISIBLE);
        addProductLotButton = findViewById(R.id.addProductLotButton);
        TabHost mTabHost = findViewById(android.R.id.tabhost);
        mTabHost.setup();
        mTabHost.addTab(mTabHost.newTabSpec("tab_test1").setIndicator(res.getString(R.string.product_detail))
                .setContent(R.id.tab1));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2").setIndicator(res.getString(R.string.stock))
                .setContent(R.id.tab2));
        mTabHost.setCurrentTab(0);
        popDialog = new AlertDialog.Builder(this);
        inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        addProductLotButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showAddProductLot();
            }
        });
        View v = findViewById(R.id.nav1);
        TextView t1 = v.findViewById(R.id.name);
        TextView t2 = v.findViewById(R.id.price);
        TextView t3 = v.findViewById(R.id.category);
        t1.setText(getString(R.string.date_added));
        t2.setText(getString(R.string.unit_price));
        t3.setText(getString(R.string.quantity));
        v.findViewById(R.id.image).setVisibility(View.GONE);
        openEditButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                edit();
            }
        });

        submitEditButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submitEdit();
            }
        });

        cancelEditButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cancelEdit();
            }
        });
        checkAdmin();
    }


    private void checkAdmin() {
        int i;
        if (Constant.isAdmin) {
            i = View.VISIBLE;
        } else {
            i = View.GONE;
        }

        openEditButton.setVisibility(i);
        findViewById(R.id.removeButton).setVisibility(i);
        addProductLotButton.setVisibility(i);
    }

    /**
     * Show list.
     *
     * @param list
     */
    private void showList(List<ProductLot> list) {

//        stockList = new ArrayList<>();
//        for (ProductLot productLot : list) {
//            stockList.add(productLot.toMap());
//        }
//
//        SimpleAdapter sAdap = new SimpleAdapter(ProductDetailActivity.this, stockList,
//                R.layout.listview_stock, new String[]{"dateAdded",
//                "cost", "quantity"}, new int[]{
//                R.id.dateAdded, R.id.cost, R.id.quantity,});

        stockListView.setAdapter(new StockAdapter(list));
    }

    @Override
    protected void onResume() {
        super.onResume();

        //stockSumBox.setText(String.valueOf(stock.getStockSumById(id)));
        showList(stock.getProductLotByProductId(id));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_edit:
                edit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Submit editing.
     */
    private void submitEdit() {
        nameBox.setFocusable(false);
        nameBox.setFocusableInTouchMode(false);
        nameBox.setBackgroundColor(getColorRes(R.color.gray));
        priceBox.setFocusable(false);
        priceBox.setFocusableInTouchMode(false);
        priceBox.setBackgroundColor(getColorRes(R.color.gray));
        descBox.setFocusable(false);
        descBox.setFocusableInTouchMode(false);
        descBox.setBackgroundColor(getColorRes(R.color.gray));
        imageBox.setOnClickListener(null);
        imageBox.setBackgroundColor(getColorRes(R.color.gray));
        barcodeBox.setFocusable(false);
        barcodeBox.setFocusableInTouchMode(false);
        barcodeBox.setBackgroundColor(getColorRes(R.color.gray));
        discountBox.setFocusable(false);
        discountBox.setFocusableInTouchMode(false);
        discountBox.setBackgroundColor(getColorRes(R.color.gray));
        taxBox.setFocusable(false);
        taxBox.setFocusableInTouchMode(false);
        taxBox.setBackgroundColor(getColorRes(R.color.gray));
        product.setName(nameBox.getText().toString());
        if (priceBox.getText().toString().equals(""))
            priceBox.setText("0.0");
        if (discountBox.getText().toString().equals(""))
            discountBox.setText("0.0");
        if (taxBox.getText().toString().equals(""))
            taxBox.setText("0.0");
        if (nameBox.getText().toString().equals(""))
            nameBox.setText(product.getName());
        if (descBox.getText().toString().equals(""))
            descBox.setText(product.getDescription());
        if (imageBox.getText().toString().equals(""))
            imageBox.setText(product.getImage());
        if (barcodeBox.getText().toString().equals(""))
            barcodeBox.setText(product.getBarcode());
        product.setImage(imageBox.getText().toString());
        product.setDescription(descBox.getText().toString());
        product.setUnitPrice(Double.parseDouble(priceBox.getText().toString()));
        product.setBarcode(barcodeBox.getText().toString());
        product.setDiscount(Double.parseDouble(discountBox.getText().toString()));
        product.setTax(Double.parseDouble(taxBox.getText().toString()));
        productCatalog.editProduct(product);
        submitEditButton.setVisibility(View.INVISIBLE);
        cancelEditButton.setVisibility(View.INVISIBLE);
        openEditButton.setVisibility(View.VISIBLE);

    }

    /**
     * Cancel editing.
     */
    private void cancelEdit() {
        nameBox.setFocusable(false);
        nameBox.setFocusableInTouchMode(false);
        nameBox.setBackgroundColor(Color.parseColor("#87CEEB"));
        priceBox.setFocusable(false);
        priceBox.setFocusableInTouchMode(false);
        priceBox.setBackgroundColor(Color.parseColor("#87CEEB"));
        barcodeBox.setFocusable(false);
        barcodeBox.setFocusableInTouchMode(false);
        barcodeBox.setBackgroundColor(Color.parseColor("#87CEEB"));
        discountBox.setFocusable(false);
        discountBox.setFocusableInTouchMode(false);
        discountBox.setBackgroundColor(Color.parseColor("#87CEEB"));
        taxBox.setFocusable(false);
        taxBox.setFocusableInTouchMode(false);
        taxBox.setBackgroundColor(Color.parseColor("#87CEEB"));
        submitEditButton.setVisibility(View.INVISIBLE);
        cancelEditButton.setVisibility(View.INVISIBLE);
        nameBox.setText(remember[0]);
        priceBox.setText(remember[1]);
        barcodeBox.setText(remember[2]);
        discountBox.setText(remember[3]);
        taxBox.setText(remember[4]);
        openEditButton.setVisibility(View.VISIBLE);
    }

    private int getColorRes(int id) {
        return ContextCompat.getColor(this, id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        imagePicker.handleActivityResult(resultCode, requestCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        imagePicker.handlePermission(requestCode, grantResults);
    }

    /**
     * Edit
     */
    private void edit() {
        nameBox.setFocusable(true);
        nameBox.setFocusableInTouchMode(true);
        nameBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        descBox.setFocusable(true);
        descBox.setFocusableInTouchMode(true);
        descBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        imageBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePicker = new ImagePicker(ProductDetailActivity.this, null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        imageBox.setText(imageUri.toString());
                    }
                });

                imagePicker.choosePicture(true);
            }
        });
        imageBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        taxBox.setFocusable(true);
        taxBox.setFocusableInTouchMode(true);
        taxBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        discountBox.setFocusable(true);
        discountBox.setFocusableInTouchMode(true);
        discountBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        priceBox.setFocusable(true);
        priceBox.setFocusableInTouchMode(true);
        priceBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        barcodeBox.setFocusable(true);
        barcodeBox.setFocusableInTouchMode(true);
        barcodeBox.setBackgroundColor(Color.parseColor("#FFBB33"));
        remember[0] = nameBox.getText().toString();
        remember[1] = priceBox.getText().toString();
        remember[2] = barcodeBox.getText().toString();
        remember[3] = discountBox.getText().toString();
        remember[4] = taxBox.getText().toString();
        submitEditButton.setVisibility(View.VISIBLE);
        cancelEditButton.setVisibility(View.VISIBLE);
        openEditButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Show adding product lot.
     */
    private void showAddProductLot() {
        Viewlayout = inflater.inflate(R.layout.layout_addproductlot,
                (ViewGroup) findViewById(R.id.addProdutlot_dialog));
        popDialog.setView(Viewlayout);

        costBox = Viewlayout.findViewById(R.id.costBox);
        quantityBox = Viewlayout.findViewById(R.id.quantityBox);
        confirmButton = Viewlayout.findViewById(R.id.confirmButton);
        clearButton = Viewlayout.findViewById(R.id.clearButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (quantityBox.getText().toString().equals("") || costBox.getText().toString().equals("")) {
                    Toast.makeText(ProductDetailActivity.this,
                            res.getString(R.string.please_input_all), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    boolean success = stock.addProductLot(
                            DateTimeStrategy.getCurrentTime(),
                            Integer.parseInt(quantityBox.getText().toString()),
                            product,
                            Double.parseDouble(costBox.getText().toString()));

                    if (success) {
                        Toast.makeText(ProductDetailActivity.this, res.getString(R.string.success), Toast.LENGTH_SHORT).show();
                        costBox.setText("");
                        quantityBox.setText("");
                        onResume();
                        alert.dismiss();


                    } else {
                        Toast.makeText(ProductDetailActivity.this, res.getString(R.string.fail), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (quantityBox.getText().toString().equals("") && costBox.getText().toString().equals("")) {
                    alert.dismiss();
                    onResume();
                } else {
                    costBox.setText("");
                    quantityBox.setText("");
                }
            }
        });

        alert = popDialog.create();
        alert.show();
    }
}