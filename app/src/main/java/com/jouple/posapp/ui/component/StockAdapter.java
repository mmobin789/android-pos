package com.jouple.posapp.ui.component;

import android.view.View;
import android.view.ViewGroup;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.ProductLot;
import com.jouple.posapp.ui.utils.Constant;

import java.util.List;

/**
 * Created by macbook on 17/11/2017.
 */

public class StockAdapter extends BaseRecyclerAdapter {
    private List<ProductLot> list;

    public StockAdapter(List<ProductLot> list) {
        this.list = list;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GeneralViewHolder(View.inflate(parent.getContext(), R.layout.inventory_list, null), GeneralViewHolder.Adapter.stock, listener);

    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        ProductLot lot = list.get(position);
        holder.name.setText(lot.getDateAdded());
        holder.category.setText(String.valueOf(lot.getQuantity()));
        holder.price.setText(Constant.decimalPlaces(lot.unitCost()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
