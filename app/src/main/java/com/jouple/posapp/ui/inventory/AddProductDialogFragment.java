package com.jouple.posapp.ui.inventory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.Category;
import com.jouple.posapp.domain.inventory.Inventory;
import com.jouple.posapp.domain.inventory.Product;
import com.jouple.posapp.domain.inventory.ProductCatalog;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.utils.SleekUI;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.util.List;

import static com.jouple.posapp.ui.component.PagerAdapter.mainUI;
import static com.jouple.posapp.ui.component.PagerAdapter.productsFragment;


/**
 * A dialog of adding a Product.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class AddProductDialogFragment extends DialogFragment {

    private EditText barcodeBox;
    private ProductCatalog productCatalog;
    private Button scanButton;
    private EditText priceBox, categoryBox, taxBox, discountBox;
    private EditText nameBox, imageBox, descBox;
    private Button confirmButton;
    private Button clearButton;
    private Resources res;
    private int selectedCatID = -1;
    private int selectedCatIndex = -1;


    public AddProductDialogFragment() {

    }

    private void showCategories(View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v);
        Menu menu = popup.getMenu();
        List<Category> filtered = productCatalog.getAllCategories();
        for (int i = 0; i < filtered.size(); i++) {
            menu.add(Menu.NONE, filtered.get(i).getId(), i, filtered.get(i).getName());

        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                categoryBox.setText(menuItem.getTitle());
                selectedCatID = menuItem.getItemId();
                selectedCatIndex = menuItem.getOrder();
                return true;
            }
        });

//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        try {
            productCatalog = Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        View v = inflater.inflate(R.layout.layout_addproduct, container,
                false);

        res = getResources();
        SleekUI.setHeader("Add Product", v);
        barcodeBox = v.findViewById(R.id.barcodeBox);
        scanButton = v.findViewById(R.id.scanButton);
        priceBox = v.findViewById(R.id.priceBox);
        imageBox = v.findViewById(R.id.image);
        descBox = v.findViewById(R.id.desc);
        categoryBox = v.findViewById(R.id.categoryBox);
        taxBox = v.findViewById(R.id.taxBox);
        discountBox = v.findViewById(R.id.discountBox);
        nameBox = v.findViewById(R.id.nameBox);
        confirmButton = v.findViewById(R.id.confirmButton);
        clearButton = v.findViewById(R.id.clearButton);

        initUI();
        return v;
    }

    /**
     * Construct a new
     */
    public void checkForCategories() {
        View.OnClickListener onClickListener;
        if (productCatalog.getAllCategories().size() == 0) {
            //categoryBox.setHint("Click here to add categories");
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    assert getActivity() != null;
                    //   ((MainActivity) getActivity()).getViewPager().setCurrentItem(0);
                    MainUI main = (MainUI) mainUI;
                    CategoryFragment categoryFragment = (CategoryFragment) main.getChildFragmentManager().getFragments().get(1);
                    categoryFragment.addCategoryDialog(view);


                }
            };

        } else {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showCategories(view);
                }
            };
        }
        categoryBox.setOnClickListener(onClickListener);
    }

    private boolean alreadyExists(String productName) {
        boolean b = false;
        for (Product product : productCatalog.getAllProduct()) {
            b = productName.equals(product.getName());
        }
        return b;
    }

    private void initUI() {
        categoryBox.setFocusable(false);
        checkForCategories();

        imageBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productsFragment.getMainActivity().initImagePicker(new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        imageBox.setText(imageUri.toString());
                    }
                });


            }
        });
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //IntentIntegratorSupportV4 scanIntegrator = new IntentIntegratorSupportV4(AddProductDialogFragment.this);
                // scanIntegrator.initiateScan();
                IntentIntegrator scanIntegrator = new IntentIntegrator(productsFragment.getMainActivity());
                scanIntegrator.initiateScan();

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String taxS = taxBox.getText().toString();
                String dis = discountBox.getText().toString();
                String name = nameBox.getText().toString();
                if (name.equals("")
                        || barcodeBox.getText().toString().equals("")
                        || priceBox.getText().toString().equals("") || categoryBox.getText().toString().equals("")
                        || descBox.getText().toString().equals("")
                        ) {

                    Toast.makeText(getContext(),
                            res.getString(R.string.please_input_all), Toast.LENGTH_SHORT)
                            .show();

                } else {
                    boolean exists = alreadyExists(name);
                    if (!exists) {
                        double tax, discount;
                        if (taxS.equals(""))
                            tax = 0;
                        else
                            tax = Double.parseDouble(taxS);
                        if (dis.equals(""))
                            discount = 0;
                        else
                            discount = Double.parseDouble(dis);

                        boolean success = productCatalog.addProduct(nameBox
                                .getText().toString(), imageBox.getText().toString(), descBox.getText().toString(), selectedCatID, barcodeBox.getText()
                                .toString(), Double.parseDouble(priceBox.getText()
                                .toString()), tax, discount);

                        if (success) {
                            Toast.makeText(getContext(),
                                    res.getString(R.string.success) + ", "
                                            + nameBox.getText().toString(),
                                    Toast.LENGTH_SHORT).show();
                            ProductsFragment.getInstance().update();
                            MainUI.refresh((MainUI) mainUI);
                            //clearAllBox();
                            //   Category category = productCatalog.getCategoryByID(selectedCatID);
                            //adapter.notifyChildInserted(selectedCatIndex, category.getChildList().size());
                            dismiss();


                        } else {
                            Toast.makeText(getContext(),
                                    res.getString(R.string.fail),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(v.getContext(), "already exists", Toast.LENGTH_SHORT).show();
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryBox.getText().toString().equals("") && barcodeBox.getText().toString().equals("") && nameBox.getText().toString().equals("") && priceBox.getText().toString().equals("")) {
                    dismiss();
                } else {
                    clearAllBox();
                }
            }
        });


    }


    /**
     * Clear all box
     */
    private void clearAllBox() {
        barcodeBox.setText("");
        nameBox.setText("");
        priceBox.setText("");
        categoryBox.setText("");
        taxBox.setText("");
        discountBox.setText("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, intent);

        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            barcodeBox.setText(scanContent);
        } else {
            Toast.makeText(getActivity(),
                    res.getString(R.string.fail),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
