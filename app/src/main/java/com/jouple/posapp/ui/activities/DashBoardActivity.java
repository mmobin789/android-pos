package com.jouple.posapp.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.sale.Admin;
import com.jouple.posapp.ui.utils.Constant;
import com.jouple.posapp.ui.utils.SleekUI;

public class DashBoardActivity extends BaseActivity {
    ImageView logOutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        loadHomePage();
        //checkAdmin();


    }

//    @Override
//    public void onBackPressed() {
//        Constant.clearAdmin(DashBoardActivity.this);
//        //logOutBtn.setVisibility(View.GONE);
//       // Toast.makeText(logOutBtn.getContext(), "Logged out", Toast.LENGTH_SHORT).show();
//        setResult(RESULT_OK);
//        super.onBackPressed();
//
//    }

//    private void checkAdmin() {
//        Constant.checkAdminControl(this);
//        if (Constant.isAdmin) {
//            logOutBtn.setVisibility(View.VISIBLE);
//
//        } else {
//            logOutBtn.setVisibility(View.GONE);
//            findViewById(R.id.tax).setEnabled(false);
//            findViewById(R.id.discount).setEnabled(false);
//            findViewById(R.id.tab).setEnabled(false);
//        }
//    }

    public void openSettings(View v) {
        startActivity(new Intent(v.getContext(), SettingsActivity.class));
    }

    public void openGeneralSettingsDialog(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_addcategory);
        SleekUI.setHeader(getString(R.string.action_settings), dialog, true);
        TextInputLayout l = dialog.findViewById(R.id.discountLayout);
        TextInputLayout t = dialog.findViewById(R.id.taxLayout);
        TextInputLayout tabLayout = dialog.findViewById(R.id.categoryLayout);
        String header = "";
        switch (view.getId()) {
            case R.id.tab:
                l.setVisibility(View.GONE);
                header = getString(R.string.action_settings);
                break;
            case R.id.discount:
                tabLayout.setVisibility(View.GONE);
                t.setVisibility(View.GONE);
                header = "Discount";
                break;
            case R.id.tax:
                t.setVisibility(View.VISIBLE);
                l.setVisibility(View.GONE);
                tabLayout.setVisibility(View.GONE);
                header = "Tax";
                break;
        }
        SleekUI.setHeader(header, dialog, true);
        t.setHint(getString(R.string.tax));
        l.setHint(getString(R.string.discount));
        tabLayout.setHint("Tab Name");
        final EditText etDiscount = l.getEditText();
        final EditText etTax = t.getEditText();
        final EditText etTab = tabLayout.getEditText();
        assert etTax != null && etDiscount != null;
        assert etTab != null;
        final Admin a = Constant.getAdmin(view.getContext());
        etTab.setText(a.getTab());
        etTax.setText("" + a.getTax() + "");
        etDiscount.setText("" + a.getDiscount() + "");
        dialog.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "";
                String taxS = etTax.getText().toString();
                String dS = etDiscount.getText().toString();
                String tabName = etTab.getText().toString();
                double tax;
                double discount;


                if (tabName.length() > 0) {
                    a.setTab(tabName);
                }
                if (taxS.length() > 0) {
                    tax = Double.parseDouble(taxS);

                    a.setTax(tax);


                }
                if (dS.length() > 0) {
                    discount = Double.parseDouble(dS);
                    a.setDiscount(discount);

                }
                if (taxS.length() > 0 || dS.length() > 0 || tabName.length() > 0) {
                    Constant.setGeneralSettings(view.getContext(), a);
                    msg = "success";
                } else msg = "Empty Fields";


                Toast.makeText(view.getContext(), msg, Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
        dialog.findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTax.setText("");
            }
        });

        dialog.show();


    }

    private void logOut(View v) {
        PopupMenu popup;
        popup = new PopupMenu(v.getContext(), v);
        Menu menu = popup.getMenu();
        menu.add(Menu.NONE, 2, Menu.NONE, "Log Out");


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 2:
                        setResult(RESULT_OK);
                        onBackPressed();
                        break;
//                    case 3:
//                        Constant.openGeneralSettingsDialog(HomePageActivity.this,S);
//                        break;
                }
                return true;
            }
        });

//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
    }

    private void openMenu(int pageNo, boolean showInventoryUIonly) {

        Intent main = new Intent(this, MainActivity.class);
        main.putExtra("pageNo", pageNo);
        main.putExtra("ui", showInventoryUIonly);
        startActivity(main);
    }

    private void loadHomePage() {
        logOutBtn = findViewById(R.id.logout);
        //logOutBtn.setTypeface(SleekUI.logoFont(3, this));
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut(view);
            }
        });
        findViewById(R.id.inventory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMenu(0, true);

            }
        });
        findViewById(R.id.newOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMenu(0, false);

            }
        });

        findViewById(R.id.reports).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMenu(1, false);
            }
        });
        findViewById(R.id.cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMenu(0, false);
            }
        });

        findViewById(R.id.refund).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), RefundActivity.class));
            }
        });

    }
}
