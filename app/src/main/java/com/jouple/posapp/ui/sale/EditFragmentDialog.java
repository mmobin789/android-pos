package com.jouple.posapp.ui.sale;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.sale.Register;
import com.jouple.posapp.techicalservices.NoDaoSetException;

import static com.jouple.posapp.ui.component.PagerAdapter.reportFragment;
import static com.jouple.posapp.ui.component.PagerAdapter.saleFragment;


/**
 * A dialog for edit a LineItem of sale,
 * overriding price or set the quantity.
 *
 * @author Refresh Team
 */
@SuppressLint("ValidFragment")
public class EditFragmentDialog extends DialogFragment {
    private Register register;
    private EditText quantityBox;
    private EditText priceBox;


    public EditFragmentDialog() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View v = inflater.inflate(R.layout.dialog_saleedit, container, false);
        try {
            register = Register.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        quantityBox = v.findViewById(R.id.quantityBox);
        priceBox = v.findViewById(R.id.priceBox);
        Button comfirmButton = v.findViewById(R.id.confirmButton);
        Button removeButton = v.findViewById(R.id.removeButton);
        assert getArguments() != null;
        final int saleId = getArguments().getInt("sale_id");

        final LineItem lineItem = getArguments().getParcelable("lineItem");
        assert lineItem != null;
        quantityBox.setText("" + lineItem.getQuantity() + "");
        priceBox.setText("" + lineItem.getProduct().getUnitPrice() + "");
        removeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.d("remove", "id=" + lineItem.getId());
                register.removeItem(lineItem);
                end();
            }
        });

        comfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String quantity = quantityBox.getText().toString();
                if (quantity.length() > 0) {
                    int q = Integer.parseInt(quantity);
                    if (q > 0) {
                        register.updateItem(
                                saleId,
                                lineItem, q
                                ,
                                Double.parseDouble(priceBox.getText().toString())
                        );


                        end();
                    } else {
                        Toast.makeText(view.getContext(), "Quantity Not Acceptable", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(view.getContext(), "Quantity Not Acceptable", Toast.LENGTH_SHORT).show();
                }
            }

        });
        return v;
    }

    /**
     * End.
     */
    private void end() {
        saleFragment.update();
        reportFragment.update();
        dismiss();
    }


}
