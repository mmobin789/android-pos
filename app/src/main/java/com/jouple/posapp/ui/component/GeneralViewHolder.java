package com.jouple.posapp.ui.component;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jouple.posapp.R;
import com.jouple.posapp.ui.utils.AppTextView;
import com.squareup.picasso.Picasso;

/**
 * Created by macbook on 09/11/2017.
 */

class GeneralViewHolder extends RecyclerView.ViewHolder {
    AppTextView name, price, category, tax, discount, quantity, productCategory;
    ImageView iv;
    private AppTextView action;


    GeneralViewHolder(View itemView, Adapter adapter, final BaseRecyclerAdapter.OnListClickListener listener) {
        super(itemView);
        switch (adapter) {
            case category:
                initCategoryViews();
                break;
            case products:
                initProductsViews();
                break;
            case sales:
                initSaleViews();
                break;
            case reports:
                initReportViews();
                break;
            case stock:
                initStockViews();
                break;
        }
        if (listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    listener.OnClick(view, getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return listener.OnLongClick(view, getAdapterPosition());
                }
            });
        }
    }

    private void initStockViews() {
        initProductsViews();
        iv.setVisibility(View.GONE);
        action.setVisibility(View.GONE);

    }

    void setOnPicasso(String path) {
        Picasso.with(itemView.getContext()).load(path).fit().centerCrop().placeholder(R.color.btn_color).into(iv);

    }

    private void initReportViews() {
        productCategory = itemView.findViewById(R.id.categoryName);
        name = itemView.findViewById(R.id.name);
        quantity = itemView.findViewById(R.id.quantity);
        tax = itemView.findViewById(R.id.tax);
        discount = itemView.findViewById(R.id.discount);
        price = itemView.findViewById(R.id.price);
        iv = itemView.findViewById(R.id.image);
        //   price.setVisibility(View.GONE);
        discount.setVisibility(View.GONE);
        iv.setVisibility(View.GONE);
        productCategory.setVisibility(View.GONE);

    }

    private void initSaleViews() {
        iv = itemView.findViewById(R.id.image);
        name = itemView.findViewById(R.id.name);
        price = itemView.findViewById(R.id.price);
        tax = itemView.findViewById(R.id.tax);
        discount = itemView.findViewById(R.id.discount);
        quantity = itemView.findViewById(R.id.quantity);
        productCategory = itemView.findViewById(R.id.categoryName);
        tax.setVisibility(View.GONE);
        iv.setVisibility(View.VISIBLE);
        name.setBoldTextEnabled(false);
        price.setBoldTextEnabled(false);
        quantity.setBoldTextEnabled(false);
        tax.setBoldTextEnabled(false);
        discount.setBoldTextEnabled(false);

    }

    private void initCategoryViews() {
        initProductsViews();
        iv.setVisibility(View.GONE);
        action.setVisibility(View.GONE);

        //   price.setVisibility(View.GONE);
        // category.setVisibility(View.GONE);
//        name.setGravity(Gravity.START);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) name.getLayoutParams();
//        params.setMarginStart(itemView.getContext().getResources().getDimensionPixelOffset(R.dimen._16sdp));
//        name.setLayoutParams(params);
        category.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        category.setBackgroundResource(R.drawable.btn_app);
        category.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) category.getLayoutParams();
        params.setMargins(0, 0, itemView.getContext().getResources().getDimensionPixelOffset(R.dimen._12sdp), 0);
        category.setLayoutParams(params);
        category.setText("View Products");

    }

    private void initProductsViews() {
        name = itemView.findViewById(R.id.name);
        price = itemView.findViewById(R.id.price);
        category = itemView.findViewById(R.id.category);

        action = itemView.findViewById(R.id.action);
        iv = itemView.findViewById(R.id.image);
        action.setVisibility(View.VISIBLE);
//        action.setTextSize(itemView.getContext().getResources().getDimensionPixelOffset(R.dimen._10ssp));
        //      action.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.btn_app));
//        action.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));


    }

    enum Adapter {
        category, products, sales, reports, stock

    }

}
