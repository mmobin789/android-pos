package com.jouple.posapp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.jouple.posapp.R;
import com.jouple.posapp.domain.inventory.LineItem;
import com.jouple.posapp.domain.sale.QuickLoadSale;
import com.jouple.posapp.domain.sale.Sale;
import com.jouple.posapp.domain.sale.SaleLedger;
import com.jouple.posapp.techicalservices.NoDaoSetException;
import com.jouple.posapp.ui.component.SalesAdapter;
import com.jouple.posapp.ui.utils.AppTextView;
import com.jouple.posapp.ui.utils.SleekUI;

import java.util.List;

/**
 * UI for showing the detail of Sale in the record.
 *
 * @author Refresh Team
 */
public class SaleDetailActivity extends BaseActivity {

    private AppTextView totalBox, dateBox;
    private RecyclerView lineitemListView;
    //private List<Map<String, String>> lineitemList;
    private Sale sale;
    private SaleLedger saleLedger;
    private QuickLoadSale quickLoadSale;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            saleLedger = SaleLedger.getInstance();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        int saleId = getIntent().getIntExtra("id", -1);
        sale = saleLedger.getSaleById(saleId);
        quickLoadSale = saleLedger.getFullSaleByID(saleId);

        initUI(savedInstanceState);
    }


    /**
     * Initiate actionbar.
     */
    @SuppressLint("NewApi")
    private void initiateActionBar() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.sale));
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33B5E5")));
        }
    }


    /**
     * Initiate this UI.
     *
     * @param savedInstanceState
     */
    private void initUI(Bundle savedInstanceState) {
        setContentView(R.layout.layout_saledetail);

//        initiateActionBar();

        totalBox = findViewById(R.id.totalBox);
        dateBox = findViewById(R.id.dateBox);
        lineitemListView = findViewById(R.id.lineitemList);
        SleekUI.initRecyclerView(lineitemListView, true);
        findViewById(R.id.tax).setVisibility(View.GONE);
        findViewById(R.id.discount).setVisibility(View.GONE);
        findViewById(R.id.image).setVisibility(View.GONE);
        findViewById(R.id.categoryName).setVisibility(View.GONE);
        findViewById(R.id.add).setVisibility(View.GONE);


    }

    /**
     * Show list.
     *
     * @param list
     */
    private void showList(List<LineItem> list) {
//        lineitemList = new ArrayList<>();
//        for (LineItem line : list) {
//            lineitemList.add(line.toMap());
//        }

        SalesAdapter adapter = new SalesAdapter(list);
        adapter.setReportDetailUI(true);
        lineitemListView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Update UI.
     */
    public void update() {
        SleekUI.getLabelView(this, R.id.taxBox).setText(String.valueOf(quickLoadSale.getTotalTax()));
        SleekUI.getLabelView(this, R.id.discountBox).setText(String.valueOf(quickLoadSale.getTotalDiscount()));
        totalBox.setText("" + sale.getTotal() + "");
        dateBox.setText("" + sale.getEndTime() + "");
        showList(sale.getAllLineItem());
    }

    @Override
    protected void onResume() {
        super.onResume();
        update();
    }
}
