package com.jouple.posapp.api

import com.google.gson.annotations.SerializedName

/**
 * Created by macbook on 15/02/2018.
 */
class ActivationCode(
        @SerializedName("auth_key")
        val activationCode: String,
        val isVerified: Boolean
)