package com.jouple.posapp.api

import android.app.Dialog
import android.util.Log
import android.widget.Button
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by macbook on 14/02/2018.
 */
class ApiCalls {
    companion object {
        private var retrofit: Retrofit? = null
        private const val baseURL = "http://reports.jouple.net/api/"


        private fun getAPI(): API {


            if (retrofit == null) {

                val client = OkHttpClient().newBuilder().connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES)
                        .writeTimeout(5, TimeUnit.MINUTES).build()

                retrofit = Retrofit.Builder()
                        .baseUrl(baseURL)
                        //   .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build()
            }

            return retrofit!!.create(API::class.java)
        }


        fun uploadReports(reportJSON: JSONObject) {
            val stringData = reportJSON.toString()
            Log.i("data", stringData)
            getAPI().uploadReports(reportJSON).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                    Log.i("ReportAPI", response?.body()?.string())


                }

                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    Log.e("ReportAPI", t.toString())
                }
            })
        }

        fun activationCode(code: String, onActivationListener: OnActivationListener, ok: Button, dialog: Dialog) {
            ok.isEnabled = false
            getAPI().activationCode(ActivationCode(code, false)).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                    val body = response?.body()?.string()
                    Log.i("activationCodeAPI", body)
                    val data = JSONObject(body)

                    val verified = data.getString("message") == "Key Match" && data.getInt("id") == 200
                    onActivationListener.onActivation(ActivationCode(code, verified), dialog)
                }

                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    Log.e("activationCodeAPI", t.toString())
                    ok.isEnabled = true
                }

            })
        }

    }


}