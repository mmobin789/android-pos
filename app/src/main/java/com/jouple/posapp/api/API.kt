package com.jouple.posapp.api

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by macbook on 14/02/2018.
 */
interface API {
    @FormUrlEncoded
    @POST("reports/invoice")
    fun uploadReports(@Field("data") report: JSONObject): Call<ResponseBody>


    @POST("auth")
    fun activationCode(@Body activationCode: ActivationCode): Call<ResponseBody>
}