package com.jouple.posapp.api

import android.app.Dialog

/**
 * Created by macbook on 15/02/2018.
 */
interface OnActivationListener {
    fun onActivation(activationCode: ActivationCode, dialogToBeDismissed: Dialog)
}